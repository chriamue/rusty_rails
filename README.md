# Rusty Rails

Rusty Rails is a simulator and framework to train dispatching agents for rail networks.

## build

Build project using cargo.

```bash
cargo build
```

### build wasm

```bash
cargo install wasm-pack
wasm-pack build
```

## test

Test using cargo.

```bash
cargo test
```

Check Test coverage using tarpaulin.

```bash
cargo install cargo-tarpaulin
cargo tarpaulin --out Html
```

## run on desktop

```bash
cargo run --bin desktop --features="ai vis" --release
```
