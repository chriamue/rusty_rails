use nalgebra::{Point2, RealField, Vector2};
use ncollide2d::shape::{Ball, Cuboid, ShapeHandle};
use nphysics2d::force_generator::DefaultForceGeneratorSet;
use nphysics2d::joint::{DefaultJointConstraintSet, RevoluteConstraint};

use nphysics2d::object::{
    BodyPartHandle, ColliderDesc, DefaultBodySet, DefaultColliderSet, Ground, RigidBodyDesc,
};
use nphysics2d::world::{DefaultGeometricalWorld, DefaultMechanicalWorld};
use std::collections::HashMap;
use std::default::Default;
use uuid::Uuid;

use generational_arena::Index;

use crate::rails::adjacency::AdjacencyRelation;
use crate::rails::rail_element::RailElementType;
use crate::rails::rail_network::RailNetwork;

macro_rules! r(
    ($e: expr) => {
        nphysics2d::nalgebra::convert::<f64, N>($e)
    }
);

pub struct PhysicsGenerator<N: RealField> {
    pub rail_network: RailNetwork,
    pub mechanical_world: DefaultMechanicalWorld<N>,
    pub geometrical_world: DefaultGeometricalWorld<N>,
    pub bodies: DefaultBodySet<N>,
    pub colliders: DefaultColliderSet<N>,
    pub joint_constraints: DefaultJointConstraintSet<N>,
    pub force_generators: DefaultForceGeneratorSet<N>,
    pub elements: HashMap<Uuid, (Index, Index)>,
}

impl<N: RealField> PhysicsGenerator<N> {
    pub fn new(rail_network: &RailNetwork) -> Self {
        let mut physics_generator = Self::default();
        physics_generator.rail_network = rail_network.clone();
        physics_generator
    }

    pub fn generate(&mut self) {
        self.generate_elements();
        self.run_simulation(10);
        //self.generate_joint_constraints();
        for _ in 0..20 {
            self.move_elements();
            self.run_simulation(3);
        }
        self.generate_joint_constraints();
        self.run_simulation(2);
    }

    pub fn generate_elements(&mut self) {
        let track = ShapeHandle::new(Ball::new(r!(1.0)));
        let switch = ShapeHandle::new(Ball::new(r!(2.0)));

        for (id, element) in &self.rail_network.elements {
            let position = element.get_position2d();
            let rb = RigidBodyDesc::<N>::new()
                .translation(Vector2::new(r!(position.x.into()), r!(position.y.into())))
                .build();
            let rb_handle = self.bodies.insert(rb);

            let co = ColliderDesc::new(match element.element_type() {
                RailElementType::TRACK => track.clone(),
                RailElementType::SWITCH => switch.clone(),
            })
            .density(r!(1.0))
            .build(BodyPartHandle(rb_handle, 0));
            let collider = self.colliders.insert(co);
            self.elements.insert(id.clone(), (rb_handle, collider));
        }
    }

    pub fn move_elements(&mut self) {
        let element_keys: Vec<Uuid> = self.rail_network.elements.keys().cloned().collect();
        for id in &element_keys {
            let element = self.rail_network.elements.get(id).unwrap();
            for adjacent in element.adjacency_list() {
                match adjacent {
                    AdjacencyRelation::Single(Some(single)) => self.move_element(id, &single),
                    AdjacencyRelation::Left(Some(left)) => self.move_element(id, &left),
                    AdjacencyRelation::Right(Some(right)) => self.move_element(id, &right),
                    _ => (),
                }
            }
        }
    }

    pub fn move_element(&mut self, first: &Uuid, second: &Uuid) {
        use nphysics2d::math::{Force, ForceType};
        let first_rb_handle = self.elements.get(first).unwrap().0;
        let second_rb_handle = self.elements.get(second).unwrap().0;

        let first_rb = self.bodies.rigid_body(first_rb_handle).unwrap();
        let second_rb = self.bodies.rigid_body(second_rb_handle).unwrap();
        let direction =
            first_rb.position().translation.vector - second_rb.position().translation.vector;

        let first_rb = self.bodies.get_mut(first_rb_handle).unwrap();
        let force = Force::linear(direction * r!(-0.2));
        first_rb.apply_force(0, &force, ForceType::VelocityChange, true);
        let second_rb = self.bodies.get_mut(second_rb_handle).unwrap();
        let force = Force::linear(direction * r!(0.2));
        second_rb.apply_force(0, &force, ForceType::VelocityChange, true);
    }

    pub fn generate_joint_constraints(&mut self) {
        let element_keys: Vec<Uuid> = self.rail_network.elements.keys().cloned().collect();
        for id in &element_keys {
            let element = self.rail_network.elements.get(id).unwrap();
            for adjacent in element.adjacency_list() {
                match adjacent {
                    AdjacencyRelation::Single(Some(single)) => self.join_constraints(id, &single),
                    AdjacencyRelation::Left(Some(left)) => self.join_constraints(id, &left),
                    AdjacencyRelation::Right(Some(right)) => self.join_constraints(id, &right),
                    _ => (),
                }
            }
        }
    }

    pub fn join_constraints(&mut self, first: &Uuid, second: &Uuid) {
        let first_rb_handle = self.elements.get(first).unwrap().0;
        let second_rb_handle = self.elements.get(second).unwrap().0;
        let rad = r!(0.2);
        let constraint = RevoluteConstraint::new(
            BodyPartHandle(first_rb_handle, 0),
            BodyPartHandle(second_rb_handle, 0),
            Point2::origin(),
            Point2::new(-rad * r!(10.0), r!(0.0)),
        );
        self.joint_constraints.insert(constraint);
    }

    pub fn run_simulation(&mut self, iterations: usize) {
        for _ in 0..iterations {
            self.mechanical_world.step(
                &mut self.geometrical_world,
                &mut self.bodies,
                &mut self.colliders,
                &mut self.joint_constraints,
                &mut self.force_generators,
            );
        }
    }

    pub fn update_positions(&mut self, rail_network: &mut RailNetwork) {
        let element_keys: Vec<Uuid> = rail_network.elements.keys().cloned().collect();
        for id in &element_keys {
            let element = rail_network.elements.get_mut(id).unwrap();
            let (body, _collider) = self.elements.get(id).unwrap();
            let body = self.bodies.rigid_body(*body).unwrap();
            let vector: Vector2<f64> = nphysics2d::nalgebra::convert::<Vector2<f64>, _>(
                nphysics2d::nalgebra::convert_unchecked(body.position().translation.vector),
            );
            element.set_position2d(Point2::<f32>::new(vector.x as f32, vector.y as f32));
        }
    }
}

impl<N: RealField> Default for PhysicsGenerator<N> {
    fn default() -> Self {
        let mut physics_generator = PhysicsGenerator {
            rail_network: RailNetwork::new(),
            mechanical_world: DefaultMechanicalWorld::new(Vector2::new(r!(0.0), r!(-9.81))),
            geometrical_world: DefaultGeometricalWorld::new(),
            bodies: DefaultBodySet::new(),
            colliders: DefaultColliderSet::new(),
            joint_constraints: DefaultJointConstraintSet::new(),
            force_generators: DefaultForceGeneratorSet::new(),
            elements: HashMap::new(),
        };
        let ground_shape = ShapeHandle::new(Cuboid::new(Vector2::new(r!(1000.0), r!(1.0))));
        let ground_handle = physics_generator.bodies.insert(Ground::new());
        let co = ColliderDesc::new(ground_shape)
            .translation(-Vector2::y())
            .build(BodyPartHandle(ground_handle, 0));
        physics_generator.colliders.insert(co);
        physics_generator
    }
}

#[test]
fn test() {
    let mut physics_generator = PhysicsGenerator::<f32>::default();
    physics_generator.generate();
}
