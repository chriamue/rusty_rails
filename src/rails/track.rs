use nalgebra::geometry::Point2;
use uuid::Uuid;

use crate::core::position2d::Position2D;
use crate::rails::adjacency::{Adjacency, AdjacencyError, AdjacencyRelation};
use crate::rails::direction::{Direction, DirectionType};
use crate::rails::rail_element::{RailElement, RailElementType};
use crate::rails::state::{State, StateType};

#[derive(Debug, Copy, Clone)]
pub struct Track {
    pub id: Uuid,
    pub position: Point2<f32>,
    left: AdjacencyRelation,
    right: AdjacencyRelation,
}

impl Track {
    pub fn new() -> Track {
        Track::new_with_id(Uuid::new_v4())
    }

    pub fn new_with_id(id: Uuid) -> Track {
        Track {
            id: id,
            position: Point2::<f32>::new(0.0, 0.0),
            left: AdjacencyRelation::Left(None),
            right: AdjacencyRelation::Right(None),
        }
    }
}

impl RailElement for Track {
    fn id(&self) -> &Uuid {
        &self.id
    }

    fn is_connected(&self) -> bool {
        match (self.left, self.right) {
            (AdjacencyRelation::Left(Some(_)), AdjacencyRelation::Right(Some(_))) => true,
            _ => false,
        }
    }

    fn connect_to(&mut self, element: &Uuid) {
        match (self.left, self.right) {
            (AdjacencyRelation::Left(None), _) => {
                self.set_adjacency(AdjacencyRelation::Left(Some(element.clone())))
                    .expect("left adjacency could not be set");
            }
            (_, AdjacencyRelation::Right(None)) => {
                self.set_adjacency(AdjacencyRelation::Right(Some(element.clone())))
                    .expect("right adjacency could not be set");
            }
            _ => (),
        }
    }

    fn replace_connection(&mut self, connected: &Uuid, replacement: Option<Uuid>) {
        match self.left {
            AdjacencyRelation::Left(Some(left)) => {
                if left == *connected {
                    self.set_adjacency(AdjacencyRelation::Left(replacement))
                        .expect("left adjacency could not be set");
                }
            }
            _ => (),
        };
        match self.right {
            AdjacencyRelation::Right(Some(right)) => {
                if right == *connected {
                    self.set_adjacency(AdjacencyRelation::Right(replacement))
                        .expect("right adjacency could not be set");
                }
            }
            _ => (),
        };
    }

    fn element_type(&self) -> RailElementType {
        RailElementType::TRACK
    }
}

impl Position2D for Track {
    fn set_position2d(&mut self, position: Point2<f32>) {
        self.position = position;
    }

    fn get_position2d(&self) -> Point2<f32> {
        self.position
    }
}

impl Adjacency for Track {
    fn adjacency_list(&self) -> Vec<AdjacencyRelation> {
        vec![self.left, self.right]
    }

    fn set_adjacency(&mut self, relation: AdjacencyRelation) -> Result<(), AdjacencyError> {
        match relation {
            AdjacencyRelation::Left(_) => {
                self.left = relation;
                Ok(())
            }
            AdjacencyRelation::Right(_) => {
                self.right = relation;
                Ok(())
            }
            _ => Err(AdjacencyError::wrong_relation(relation)),
        }
    }

    fn get_adjacency(
        &self,
        relation: AdjacencyRelation,
    ) -> Result<AdjacencyRelation, AdjacencyError> {
        match relation {
            AdjacencyRelation::Left(_) => Ok(self.left),
            AdjacencyRelation::Right(_) => Ok(self.right),
            _ => Err(AdjacencyError::wrong_relation(relation)),
        }
    }
}

impl State for Track {
    fn all_states(&self) -> Vec<StateType> {
        vec![]
    }

    fn possible_states(&self) -> Vec<StateType> {
        vec![]
    }

    fn get_state(&self) -> Option<StateType> {
        None
    }

    fn change_state(&mut self, _state: StateType) {}
}

impl Direction for Track {
    fn front(&self, direction: DirectionType) -> Uuid {
        let (current, front) = direction;
        if current == self.id {
            match self.left {
                AdjacencyRelation::Left(Some(left)) => {
                    if front == left {
                        return left;
                    };
                }
                _ => (),
            }
            match self.right {
                AdjacencyRelation::Right(Some(right)) => {
                    if front == right {
                        return right;
                    };
                }
                _ => (),
            }
            return Uuid::nil();
        } else {
            return Uuid::nil();
        }
    }

    fn rear(&self, direction: DirectionType) -> Uuid {
        let (current, front) = direction;
        if current == self.id {
            match (self.left, self.right) {
                (AdjacencyRelation::Left(Some(left)), AdjacencyRelation::Right(Some(right))) => {
                    if front == left {
                        return right;
                    } else if front == right {
                        return left;
                    }
                }
                _ => (),
            }
            return Uuid::nil();
        } else {
            return Uuid::nil();
        }
    }
}

unsafe impl Send for Track {}

#[test]
fn test() {
    let id: Uuid = Uuid::new_v4();

    let mut track: Track = Track::new();
    track.id = id;

    assert_eq!(track.id(), &id);
    assert_eq!(track.is_connected(), false);
    assert_eq!(track.element_type(), RailElementType::TRACK);

    let id1 = Uuid::new_v4();
    let id2 = Uuid::new_v4();

    let left = AdjacencyRelation::Left(Some(id1));

    assert!(track.set_adjacency(left).is_ok());
    assert!(track
        .set_adjacency(AdjacencyRelation::Right(Some(id2)))
        .is_ok());

    assert_eq!(track.is_connected(), true);

    assert!(track.set_adjacency(AdjacencyRelation::Left(None)).is_ok());
    assert_eq!(track.is_connected(), false);

    track.connect_to(&id1);
    assert_eq!(track.is_connected(), true);

    assert!(track.set_adjacency(AdjacencyRelation::Right(None)).is_ok());
    assert_eq!(track.is_connected(), false);

    track.connect_to(&id2);
    assert_eq!(track.is_connected(), true);

    track.replace_connection(&id2, Some(id1));
    assert_eq!(
        track.get_adjacency(AdjacencyRelation::Right(None)).unwrap(),
        AdjacencyRelation::Right(Some(id1))
    );
}

#[test]
fn test_direction() {
    let id = Uuid::new_v4();
    let left = Uuid::new_v4();
    let right = Uuid::new_v4();
    let mut track = Track::new_with_id(id);
    assert!(track
        .set_adjacency(AdjacencyRelation::Left(Some(left)))
        .is_ok());
    assert_eq!(track.front((id, left)), left);
    assert_eq!(track.rear((id, left)), Uuid::nil());
    assert_eq!(track.rear((left, id)), Uuid::nil());

    assert!(track
        .set_adjacency(AdjacencyRelation::Right(Some(right)))
        .is_ok());
    assert_eq!(track.rear((id, left)), right);
    assert_eq!(track.rear((id, right)), left);

    assert_eq!(track.front((id, Uuid::nil())), Uuid::nil());
    assert_eq!(track.rear((id, Uuid::nil())), Uuid::nil());

    assert!(track
        .set_adjacency(AdjacencyRelation::Single(Some(right)))
        .is_err());
}

#[test]
fn test_state() {
    let id = Uuid::new_v4();
    let mut track = Track::new_with_id(id);
    assert_eq!(track.all_states().len(), 0);
    assert_eq!(track.possible_states().len(), 0);
    track.change_state(StateType::Left);
    assert!(track.get_state().is_none());
}
