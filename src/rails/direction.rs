use petgraph::Direction::{Incoming, Outgoing};
use uuid::Uuid;

use crate::rails::rail_network::RailNetwork;

#[derive(Debug)]
pub enum NodeDirection {
    FORWARD,
    BACKWARD,
}

// (current, front)
pub type DirectionType = (Uuid, Uuid);

pub trait Direction {
    fn front(&self, direction: DirectionType) -> Uuid;
    fn rear(&self, direction: DirectionType) -> Uuid;
}

impl Direction for RailNetwork {
    fn front(&self, direction: DirectionType) -> Uuid {
        let d1_index = self.node_indices.get(&direction.0);
        let d2_index = self.node_indices.get(&direction.1);
        let index = match (d1_index, d2_index) {
            (Some(d1_index), Some(d2_index)) => {
                match (
                    self.network.contains_edge(d1_index.0, d2_index.0),
                    self.network.contains_edge(d1_index.0, d2_index.1),
                    self.network.contains_edge(d1_index.1, d2_index.0),
                    self.network.contains_edge(d1_index.1, d2_index.1),
                ) {
                    (true, _, _, _) => self.network.neighbors_directed(d2_index.0, Outgoing).next(),
                    (_, true, _, _) => self.network.neighbors_directed(d2_index.1, Outgoing).next(),
                    (_, _, true, _) => self.network.neighbors_directed(d2_index.0, Outgoing).next(),
                    (_, _, _, true) => self.network.neighbors_directed(d2_index.1, Outgoing).next(),
                    (_, _, _, _) => None,
                }
            }
            (Some(d1_index), None) => self.network.neighbors_directed(d1_index.0, Outgoing).next(),
            (None, Some(d2_index)) => self.network.neighbors_directed(d2_index.0, Outgoing).next(),
            (_, _) => None,
        };
        match index {
            Some(index) => self.element_by_index(&index),
            _ => Uuid::nil(),
        }
    }

    fn rear(&self, direction: DirectionType) -> Uuid {
        let d1_index = self.node_indices.get(&direction.0);
        let d2_index = self.node_indices.get(&direction.1);
        let index = match (d1_index, d2_index) {
            (Some(d1_index), Some(d2_index)) => {
                match (
                    self.network.contains_edge(d1_index.0, d2_index.0),
                    self.network.contains_edge(d1_index.0, d2_index.1),
                    self.network.contains_edge(d1_index.1, d2_index.0),
                    self.network.contains_edge(d1_index.1, d2_index.1),
                ) {
                    (true, _, _, _) => self.network.neighbors_directed(d1_index.0, Incoming).next(),
                    (_, true, _, _) => self.network.neighbors_directed(d2_index.0, Incoming).next(),
                    (_, _, true, _) => self.network.neighbors_directed(d2_index.1, Incoming).next(),
                    (_, _, _, true) => self.network.neighbors_directed(d2_index.1, Incoming).next(),
                    (_, _, _, _) => None,
                }
            }
            (Some(d1_index), None) => self.network.neighbors_directed(d1_index.0, Incoming).next(),
            (None, Some(d2_index)) => self.network.neighbors_directed(d2_index.0, Incoming).next(),
            (_, _) => None,
        };
        match index {
            Some(index) => self.element_by_index(&index),
            _ => Uuid::nil(),
        }
    }
}

#[test]
fn test_rail_network() {
    use crate::rails::rail_element::RailElement;
    use crate::rails::track::Track;

    let mut rail_network: RailNetwork = RailNetwork::new();
    let track = Track::new();
    let track2 = Track::new();
    let track3 = Track::new();
    let track4 = Track::new();
    rail_network.add_track(track);
    rail_network.add_track(track2);
    rail_network.add_track(track3);
    rail_network.add_track(track4);

    assert_eq!(rail_network.front((*track.id(), *track2.id())), Uuid::nil());
    assert_eq!(rail_network.rear((*track.id(), *track2.id())), Uuid::nil());
    assert_eq!(rail_network.front((*track.id(), Uuid::nil())), Uuid::nil());
    assert_eq!(rail_network.rear((*track.id(), Uuid::nil())), Uuid::nil());

    rail_network.connect_tracks(track.id(), track2.id());
    rail_network.connect_tracks(track2.id(), track3.id());
    rail_network.connect_tracks(track3.id(), track4.id());
    assert_eq!(
        rail_network.front((*track.id(), *track2.id())),
        *track3.id()
    );
    assert_eq!(
        rail_network.front((*track2.id(), *track3.id())),
        *track4.id()
    );
    assert_eq!(
        rail_network.rear((*track3.id(), *track4.id())),
        *track2.id()
    );
    assert_eq!(rail_network.rear((*track2.id(), *track3.id())), *track.id());
}
