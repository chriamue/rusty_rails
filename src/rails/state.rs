#[derive(Debug, Clone, Copy, Eq, Hash, PartialEq)]
pub enum StateType {
    Left,
    Right,
}

impl StateType {
    pub fn from_usize(i: usize) -> (StateType, usize) {
        match i % 2 {
            0 => (StateType::Left, i / 2),
            1 => (StateType::Right, i / 2),
            _ => panic!("Unknown state {}!", i),
        }
    }
}

pub trait State {
    fn all_states(&self) -> Vec<StateType>;
    fn possible_states(&self) -> Vec<StateType>;
    fn get_state(&self) -> Option<StateType>;
    fn change_state(&mut self, state: StateType);
}

#[test]
fn test_state_type() {
    assert_eq!(StateType::from_usize(0), (StateType::Left, 0));
    assert_eq!(StateType::from_usize(3), (StateType::Right, 1));
}
