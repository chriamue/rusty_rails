use uuid::Uuid;

use nalgebra::geometry::Point2;

use crate::core::position2d::Position2D;
use crate::rails::adjacency::{Adjacency, AdjacencyError, AdjacencyRelation};
use crate::rails::direction::{Direction, DirectionType};
use crate::rails::rail_element::{RailElement, RailElementType};
use crate::rails::state::{State, StateType};

#[derive(Debug, Copy, Clone)]
pub struct Switch {
    pub id: Uuid,
    pub position: Point2<f32>,
    left: AdjacencyRelation,
    right: AdjacencyRelation,
    single: AdjacencyRelation,
    state: StateType,
}

impl Switch {
    pub fn new() -> Switch {
        Switch::new_with_id(Uuid::new_v4())
    }

    pub fn new_with_id(id: Uuid) -> Switch {
        Switch {
            id: id,
            position: Point2::<f32>::new(0.0, 0.0),
            left: AdjacencyRelation::Left(None),
            right: AdjacencyRelation::Right(None),
            single: AdjacencyRelation::Single(None),
            state: StateType::Left,
        }
    }
}

impl RailElement for Switch {
    fn id(&self) -> &Uuid {
        &self.id
    }

    fn is_connected(&self) -> bool {
        match (self.single, self.left, self.right) {
            (
                AdjacencyRelation::Single(Some(_)),
                AdjacencyRelation::Left(Some(_)),
                AdjacencyRelation::Right(Some(_)),
            ) => true,
            _ => false,
        }
    }

    fn connect_to(&mut self, element: &Uuid) {
        match (self.single, self.left, self.right) {
            (AdjacencyRelation::Single(None), _, _) => {
                self.set_adjacency(AdjacencyRelation::Single(Some(element.clone())))
                    .expect("single adjacency could not be set");
            }
            (_, AdjacencyRelation::Left(None), _) => {
                self.set_adjacency(AdjacencyRelation::Left(Some(element.clone())))
                    .expect("left adjacency could not be set");
            }
            (_, _, AdjacencyRelation::Right(None)) => {
                self.set_adjacency(AdjacencyRelation::Right(Some(element.clone())))
                    .expect("right adjacency could not be set");
            }
            _ => (),
        }
    }

    fn replace_connection(&mut self, connected: &Uuid, replacement: Option<Uuid>) {
        match self.single {
            AdjacencyRelation::Single(Some(single)) => {
                if single == *connected {
                    self.set_adjacency(AdjacencyRelation::Single(replacement))
                        .expect("single adjacency could not be set");
                }
            }
            _ => (),
        };
        match self.left {
            AdjacencyRelation::Left(Some(left)) => {
                if left == *connected {
                    self.set_adjacency(AdjacencyRelation::Left(replacement))
                        .expect("left adjacency could not be set");
                }
            }
            _ => (),
        };
        match self.right {
            AdjacencyRelation::Right(Some(right)) => {
                if right == *connected {
                    self.set_adjacency(AdjacencyRelation::Right(replacement))
                        .expect("right adjacency could not be set");
                }
            }
            _ => (),
        };
    }

    fn element_type(&self) -> RailElementType {
        RailElementType::SWITCH
    }
}

impl Position2D for Switch {
    fn set_position2d(&mut self, position: Point2<f32>) {
        self.position = position;
    }

    fn get_position2d(&self) -> Point2<f32> {
        self.position
    }
}

impl Adjacency for Switch {
    fn adjacency_list(&self) -> Vec<AdjacencyRelation> {
        vec![self.single, self.left, self.right]
    }

    fn set_adjacency(&mut self, relation: AdjacencyRelation) -> Result<(), AdjacencyError> {
        match relation {
            AdjacencyRelation::Single(_) => {
                self.single = relation;
                Ok(())
            }
            AdjacencyRelation::Left(_) => {
                self.left = relation;
                Ok(())
            }
            AdjacencyRelation::Right(_) => {
                self.right = relation;
                Ok(())
            }
        }
    }

    fn get_adjacency(
        &self,
        relation: AdjacencyRelation,
    ) -> Result<AdjacencyRelation, AdjacencyError> {
        match relation {
            AdjacencyRelation::Single(_) => Ok(self.left),
            AdjacencyRelation::Left(_) => Ok(self.left),
            AdjacencyRelation::Right(_) => Ok(self.right),
        }
    }
}

impl State for Switch {
    fn all_states(&self) -> Vec<StateType> {
        vec![StateType::Left, StateType::Right]
    }

    fn possible_states(&self) -> Vec<StateType> {
        match self.state {
            StateType::Left => vec![StateType::Right],
            StateType::Right => vec![StateType::Left],
        }
    }

    fn get_state(&self) -> Option<StateType> {
        Some(self.state)
    }

    fn change_state(&mut self, state: StateType) {
        self.state = state;
    }
}

impl Direction for Switch {
    fn front(&self, direction: DirectionType) -> Uuid {
        let (current, front) = direction;
        return if current == self.id {
            match (self.single, self.left, self.right) {
                (
                    AdjacencyRelation::Single(Some(single)),
                    AdjacencyRelation::Left(Some(left)),
                    AdjacencyRelation::Right(Some(right)),
                ) => {
                    if front == single {
                        return single;
                    } else {
                        match self.state {
                            StateType::Left => left,
                            StateType::Right => right,
                        }
                    }
                }
                _ => Uuid::nil(),
            }
        } else {
            Uuid::nil()
        };
    }

    fn rear(&self, direction: DirectionType) -> Uuid {
        let (current, front) = direction;
        return if current == self.id {
            match (self.single, self.left, self.right) {
                (
                    AdjacencyRelation::Single(Some(single)),
                    AdjacencyRelation::Left(Some(left)),
                    AdjacencyRelation::Right(Some(right)),
                ) => {
                    if front == left || front == right {
                        single
                    } else if front == single {
                        match self.state {
                            StateType::Left => left,
                            StateType::Right => right,
                        }
                    } else {
                        Uuid::nil()
                    }
                }
                _ => Uuid::nil(),
            }
        } else {
            Uuid::nil()
        };
    }
}

unsafe impl Send for Switch {}

#[test]
fn test() {
    let id: Uuid = Uuid::new_v4();

    let mut switch: Switch = Switch::new();
    switch.id = id;

    assert_eq!(switch.id(), &id);
    assert_eq!(switch.is_connected(), false);
    assert_eq!(switch.element_type(), RailElementType::SWITCH);
}

#[test]
fn test_direction() {
    let id = Uuid::new_v4();
    let single = Uuid::new_v4();
    let left = Uuid::new_v4();
    let right = Uuid::new_v4();
    let mut switch = Switch::new_with_id(id);
    assert_eq!(switch.get_state(), Some(StateType::Left));
    assert_eq!(switch.front((left, id)), Uuid::nil());
    assert_eq!(switch.rear((left, id)), Uuid::nil());

    assert!(switch.set_adjacency(AdjacencyRelation::Single(Some(single))).is_ok());
    assert!(switch.set_adjacency(AdjacencyRelation::Left(Some(left))).is_ok());
    assert!(switch.set_adjacency(AdjacencyRelation::Right(Some(right))).is_ok());

    assert_eq!(switch.front((id, left)), left);
    assert_eq!(switch.front((id, right)), left);
    assert_eq!(switch.rear((id, left)), single);
    assert_eq!(switch.rear((left, id)), Uuid::nil());
    assert_eq!(switch.rear((id, right)), single);

    switch.change_state(StateType::Right);
    assert_eq!(switch.front((id, left)), right);
    assert_eq!(switch.front((id, right)), right);
    assert_eq!(switch.rear((id, single)), right);
}

#[test]
fn test_state() {
    let id = Uuid::new_v4();
    let mut switch = Switch::new_with_id(id);
    assert_eq!(switch.all_states().len(), 2);
    assert_eq!(switch.possible_states().len(), 1);
    switch.change_state(StateType::Right);
    assert_eq!(switch.get_state(), Some(StateType::Right));
    assert_eq!(switch.possible_states().len(), 1);
}
