use std::vec::Vec;
use uuid::Uuid;

use petgraph::algo::astar;
use petgraph::prelude::NodeIndex;

use crate::rails::adjacency::AdjacencyRelation;
use crate::rails::rail_network::RailNetwork;
use crate::rails::state::StateType;

pub type Path = Vec<Uuid>;

pub trait PathSearch {
    fn shortest_path(&self, from: &Uuid, from: &Uuid) -> Option<Path>;
}

impl PathSearch for RailNetwork {
    fn shortest_path(&self, from: &Uuid, to: &Uuid) -> Option<Path> {
        let from_nodes = self.node_indices.get(from);
        let to_nodes = self.node_indices.get(to);
        match (from_nodes, to_nodes) {
            (Some(from), Some(to)) => {
                let path1 = self.shortest_path_of_nodes(from.0, to.0);
                let path2 = self.shortest_path_of_nodes(from.0, to.1);
                let path3 = self.shortest_path_of_nodes(from.1, to.0);
                let path4 = self.shortest_path_of_nodes(from.1, to.1);
                let mut shortest_path: Option<Path> = None;
                for path in vec![path1, path2, path3, path4] {
                    match (shortest_path.clone(), path) {
                        (Some(s_path), Some(path)) => {
                            if s_path.len() > path.len() {
                                shortest_path = Some(path);
                            }
                        }
                        (None, Some(path)) => shortest_path = Some(path),
                        _ => (),
                    };
                }
                shortest_path
            }
            _ => None,
        }
    }
}

impl RailNetwork {
    fn shortest_path_of_nodes(&self, from: NodeIndex, to: NodeIndex) -> Option<Path> {
        let path = match astar(
            &self.network,
            from,
            |finish| finish == to,
            |e| *e.weight(),
            |_| 0,
        ) {
            Some(path) => Some(path.1),
            None => None,
        };
        match path {
            Some(path) => Some(
                path.into_iter()
                    .map(|e| self.element_by_index(&e))
                    .collect(),
            ),
            None => None,
        }
    }

    pub fn switches_on_path(&self, path: &Path) -> Path {
        let swich_ids = self.get_switch_ids();
        let switches: Path = path
            .iter()
            .filter(|e| swich_ids.contains(e))
            .cloned()
            .collect();
        switches
    }

    pub fn switches_set_right_in_path(&self, path: &Path) -> u32 {
        let mut counter: u32 = 0;
        let switches = self.switches_on_path(path);
        for switch_id in switches {
            let switch = self.elements.get(&switch_id).unwrap();
            match switch.get_state() {
                Some(StateType::Left) => {
                    match switch.get_adjacency(AdjacencyRelation::Left(None)) {
                        Ok(AdjacencyRelation::Left(Some(left))) => {
                            if path.contains(&left) {
                                counter = counter + 1;
                            }
                        }
                        _ => (),
                    }
                }
                Some(StateType::Right) => {
                    match switch.get_adjacency(AdjacencyRelation::Right(None)) {
                        Ok(AdjacencyRelation::Right(Some(right))) => {
                            if path.contains(&right) {
                                counter = counter + 1;
                            }
                        }
                        _ => (),
                    }
                }
                _ => (),
            }
        }
        counter
    }
}

#[test]
fn test() {
    let network = RailNetwork::default();
    let track1 = &network.get_element_ids()[0];
    let track2 = &network.get_element_ids()[2];
    let path: Path = network.shortest_path(track1, track2).unwrap();
    assert_eq!(path.len(), 3);
}

#[test]
fn test_switch_on_path() {
    use crate::rails::rail_element::RailElement;
    use crate::rails::switch::Switch;
    use crate::rails::track::Track;

    let mut network = RailNetwork::new();
    let track1 = Track::new();
    let track2 = Track::new();
    let track3 = Track::new();
    let switch1 = Switch::new();
    let switch2 = Switch::new();

    network.add_track(track1);
    network.add_track(track2);
    network.add_track(track3);
    network.add_switch(switch1);
    network.add_switch(switch2);

    network.connect_switch(switch1.id(), track1.id());
    network.connect_switch(switch1.id(), track2.id());
    network.connect_switch(switch2.id(), track2.id());
    network.connect_switch(switch2.id(), track3.id());

    let path: Path = network.shortest_path(track1.id(), track3.id()).unwrap();
    assert_eq!(path.len(), 5);

    let switches_on_path = network.switches_on_path(&path);
    assert_eq!(switches_on_path, vec![*switch1.id(), *switch2.id()]);

    assert_eq!(network.switches_set_right_in_path(&path), 2);

    let track4 = Track::new();
    network.add_track(track4);
    network.connect_switch(switch1.id(), track4.id());
    network
        .elements
        .get_mut(switch1.id())
        .unwrap()
        .change_state(StateType::Right);

    assert_eq!(network.switches_set_right_in_path(&path), 1);

    let track5 = Track::new();
    network.add_track(track5);
    network.connect_switch(switch2.id(), track5.id());
    network
        .elements
        .get_mut(switch2.id())
        .unwrap()
        .change_state(StateType::Right);

    assert_eq!(network.switches_set_right_in_path(&path), 0);
}
