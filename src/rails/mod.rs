//! # Rails
//!
//! Module that contains functions to build a rail network.
//!
pub mod adjacency;
pub mod direction;
pub mod distance;
pub mod generator;
pub mod path_search;
pub mod physics_generator;
pub mod rail_element;
pub mod rail_network;
pub mod state;
pub mod switch;
pub mod track;
