use uuid::Uuid;

use crate::rails::path_search::PathSearch;
use crate::rails::rail_network::RailNetwork;

pub trait Distance {
    fn distance(&self, from: &Uuid, to: &Uuid) -> Option<u32>;
}

impl Distance for RailNetwork {
    fn distance(&self, from: &Uuid, to: &Uuid) -> Option<u32> {
        match self.shortest_path(from, to) {
            Some(path) => Some(path.len() as u32 - 1),
            None => None,
        }
    }
}

#[test]
fn test() {
    use crate::rails::track::Track;
    let mut network = RailNetwork::new();
    let track1 = Track::new();
    let track2 = Track::new();
    network.add_track(track1);
    network.add_track(track2);
    network.connect_tracks(&track1.id, &track2.id);
    println!("{:?}", network);
    assert_eq!(network.distance(&track1.id, &track2.id), Some(1));
}
