use rand::rngs::StdRng;
use rand::seq::SliceRandom;
use rand::SeedableRng;
use std::collections::HashMap;
use uuid::Uuid;

use petgraph::dot::{Config, Dot};
use petgraph::prelude::NodeIndex;
use petgraph::{
    Direction::{Incoming, Outgoing},
    Graph,
};

use crate::rails::adjacency::AdjacencyRelation;
use crate::rails::direction::NodeDirection;
use crate::rails::rail_element::RailElement;
use crate::rails::rail_element::RailElementType;
use crate::rails::switch::Switch;
use crate::rails::track::Track;

#[derive(Debug)]
pub struct RailNetwork {
    pub elements: HashMap<Uuid, Box<dyn RailElement>>,
    pub network: Graph<Uuid, u16>,
    pub node_indices: HashMap<Uuid, (NodeIndex, NodeIndex)>,
    pub rand: StdRng,
}

impl RailNetwork {
    pub fn new() -> RailNetwork {
        RailNetwork {
            elements: HashMap::new(),
            network: Graph::<Uuid, u16>::new(),
            node_indices: HashMap::new(),
            rand: StdRng::seed_from_u64(42),
        }
    }

    pub fn get_element_ids(&self) -> Vec<Uuid> {
        let mut elements: Vec<Uuid> = self.elements.keys().cloned().collect();
        elements.sort();
        elements
    }

    pub fn get_track_ids(&self) -> Vec<Uuid> {
        let mut track_ids: Vec<Uuid> = self
            .elements
            .values()
            .filter(|e| e.element_type() == RailElementType::TRACK)
            .map(|e| e.id())
            .cloned()
            .collect();
        track_ids.sort();
        track_ids
    }

    pub fn get_switch_ids(&self) -> Vec<Uuid> {
        let mut switch_ids: Vec<Uuid> = self
            .elements
            .values()
            .filter(|e| e.element_type() == RailElementType::SWITCH)
            .map(|e| e.id())
            .cloned()
            .collect();
        switch_ids.sort();
        switch_ids
    }

    pub fn add_switch(&mut self, switch: Switch) {
        let forward_index = self.network.add_node(*switch.id());
        let backward_index = self.network.add_node(*switch.id());
        self.node_indices
            .insert(*switch.id(), (forward_index, backward_index));
        self.elements.insert(*switch.id(), Box::new(switch));
    }

    pub fn add_track(&mut self, track: Track) {
        let forward_index = self.network.add_node(*track.id());
        let backward_index = self.network.add_node(*track.id());
        self.node_indices
            .insert(*track.id(), (forward_index, backward_index));
        self.elements.insert(*track.id(), Box::new(track));
    }

    pub fn insert_track(&mut self, track: &Uuid, append_to_track_id: &Uuid) {
        // let components = connected_components(&self.network);
        let append_to_track = self.elements.get(append_to_track_id).unwrap();

        let connected_id = match append_to_track.get_adjacency(AdjacencyRelation::Left(None)) {
            Ok(AdjacencyRelation::Left(Some(uuid))) => uuid,
            Err(_) => Uuid::nil(),
            _ => Uuid::nil(),
        };

        if connected_id == Uuid::nil() {
            return;
        }

        match self.elements.get(&connected_id).unwrap().element_type() {
            RailElementType::TRACK => {
                assert_eq!(self.elements[append_to_track_id].is_connected(), true);
                assert_eq!(self.elements[&connected_id].is_connected(), true);
                assert_eq!(self.elements[track].is_connected(), false);
                self.disconnect_tracks(&connected_id, append_to_track_id);
                assert_eq!(self.elements[append_to_track_id].is_connected(), false);
                assert_eq!(self.elements[&connected_id].is_connected(), false);
                self.connect_tracks(track, append_to_track_id);
                assert_eq!(self.elements[append_to_track_id].is_connected(), true);
                self.connect_tracks(&connected_id, track);
                assert_eq!(self.elements[&connected_id].is_connected(), true);
                assert_eq!(self.elements[track].is_connected(), true);
            }
            RailElementType::SWITCH => {
                assert_eq!(self.elements[append_to_track_id].is_connected(), true);
                assert_eq!(self.elements[&connected_id].is_connected(), true);
                assert_eq!(self.elements[track].is_connected(), false);
                self.disconnect_switch(&connected_id, append_to_track_id);
                assert_eq!(self.elements[track].is_connected(), false);
                self.connect_tracks(append_to_track_id, track);
                assert_eq!(self.elements[append_to_track_id].is_connected(), true);
                self.connect_switch(&connected_id, track);
                assert_eq!(self.elements[&connected_id].is_connected(), true);
                assert_eq!(self.elements[track].is_connected(), true);
            }
        };
        // assert_eq!(connected_components(&self.network), components);
    }

    fn remove_edge(&mut self, node1: NodeIndex, node2: NodeIndex) {
        match self.network.find_edge(node1, node2) {
            Some(edge) => {
                self.network.remove_edge(edge);
            }
            _ => (),
        };
    }

    pub fn disconnect_tracks(&mut self, track1: &Uuid, track2: &Uuid) {
        self.elements
            .get_mut(track1)
            .unwrap()
            .replace_connection(track2, None);
        self.elements
            .get_mut(track2)
            .unwrap()
            .replace_connection(track1, None);
        assert_eq!(self.elements[track1].is_connected(), false);
        assert_eq!(self.elements[track2].is_connected(), false);
        let track1_index = self.node_indices[track1];
        let track2_index = self.node_indices[track2];
        self.remove_edge(track1_index.0, track2_index.0);
        self.remove_edge(track1_index.0, track2_index.1);
        self.remove_edge(track1_index.1, track2_index.0);
        self.remove_edge(track1_index.1, track2_index.1);

        self.remove_edge(track2_index.0, track1_index.0);
        self.remove_edge(track2_index.0, track1_index.1);
        self.remove_edge(track2_index.1, track1_index.0);
        self.remove_edge(track2_index.1, track1_index.1);
    }

    pub fn disconnect_switch(&mut self, switch: &Uuid, track: &Uuid) {
        self.elements
            .get_mut(switch)
            .unwrap()
            .replace_connection(track, None);
        self.elements
            .get_mut(track)
            .unwrap()
            .replace_connection(switch, None);
        assert_eq!(self.elements[switch].is_connected(), false);
        assert_eq!(self.elements[track].is_connected(), false);
        let switch_index = self.node_indices[switch];
        let track_index = self.node_indices[track];
        self.remove_edge(switch_index.0, track_index.0);
        self.remove_edge(switch_index.0, track_index.1);
        self.remove_edge(switch_index.1, track_index.0);
        self.remove_edge(switch_index.1, track_index.1);

        self.remove_edge(track_index.0, switch_index.0);
        self.remove_edge(track_index.0, switch_index.1);
        self.remove_edge(track_index.1, switch_index.0);
        self.remove_edge(track_index.1, switch_index.1);
    }

    pub fn connect_tracks(&mut self, track1: &Uuid, track2: &Uuid) {
        self.elements.get_mut(track1).unwrap().connect_to(track2);
        self.elements.get_mut(track2).unwrap().connect_to(track1);
        let track1_index = self.node_indices[track1];
        let track2_index = self.node_indices[track2];
        match self.track_direction(track1) {
            Some(NodeDirection::FORWARD) | None => match self.track_direction(track2) {
                Some(NodeDirection::FORWARD) | None => {
                    self.network.add_edge(track1_index.1, track2_index.0, 1);
                    self.network.add_edge(track2_index.1, track1_index.0, 1);
                    assert!(
                        self.network
                            .neighbors_directed(track1_index.0, Outgoing)
                            .count()
                            <= 1
                    );
                    assert!(
                        self.network
                            .neighbors_directed(track1_index.1, Outgoing)
                            .count()
                            <= 1
                    );
                }
                Some(NodeDirection::BACKWARD) => {
                    self.network.add_edge(track1_index.1, track2_index.1, 1);
                    self.network.add_edge(track2_index.0, track1_index.0, 1);
                    assert!(
                        self.network
                            .neighbors_directed(track1_index.0, Outgoing)
                            .count()
                            <= 1
                    );
                    assert!(
                        self.network
                            .neighbors_directed(track1_index.1, Outgoing)
                            .count()
                            <= 1
                    );
                }
            },
            Some(NodeDirection::BACKWARD) => match self.track_direction(track2) {
                Some(NodeDirection::FORWARD) | None => {
                    self.network.add_edge(track1_index.0, track2_index.0, 1);
                    self.network.add_edge(track2_index.1, track1_index.1, 1);
                    assert!(
                        self.network
                            .neighbors_directed(track1_index.0, Outgoing)
                            .count()
                            <= 1
                    );
                    assert!(
                        self.network
                            .neighbors_directed(track1_index.1, Outgoing)
                            .count()
                            <= 1
                    );
                }
                Some(NodeDirection::BACKWARD) => {
                    self.network.add_edge(track1_index.0, track2_index.1, 1);
                    self.network.add_edge(track2_index.0, track1_index.1, 1);
                    assert!(
                        self.network
                            .neighbors_directed(track1_index.0, Outgoing)
                            .count()
                            <= 1
                    );
                    assert!(
                        self.network
                            .neighbors_directed(track1_index.1, Outgoing)
                            .count()
                            <= 1
                    );
                }
            },
        }
    }

    pub fn connect_switch(&mut self, switch: &Uuid, track: &Uuid) {
        self.elements.get_mut(switch).unwrap().connect_to(track);
        self.elements.get_mut(track).unwrap().connect_to(switch);
        let switch_index = self.node_indices[switch];
        let track_index = self.node_indices[track];

        match (
            self.network
                .neighbors_directed(switch_index.0, Outgoing)
                .count(),
            self.network
                .neighbors_directed(switch_index.0, Incoming)
                .count(),
        ) {
            (0, 0) | (0, 1) | (0, 2) => match self.track_direction(track) {
                Some(NodeDirection::FORWARD) | None => {
                    self.network.add_edge(switch_index.0, track_index.0, 1);
                    self.network.add_edge(track_index.1, switch_index.1, 1);
                }
                Some(NodeDirection::BACKWARD) => {
                    self.network.add_edge(switch_index.0, track_index.1, 1);
                    self.network.add_edge(track_index.0, switch_index.1, 1);
                }
            },
            _ => match self.track_direction(track) {
                Some(NodeDirection::FORWARD) | None => {
                    self.network.add_edge(switch_index.1, track_index.0, 1);
                    self.network.add_edge(track_index.1, switch_index.0, 1);
                }
                Some(NodeDirection::BACKWARD) => {
                    self.network.add_edge(switch_index.1, track_index.1, 1);
                    self.network.add_edge(track_index.0, switch_index.0, 1);
                }
            },
        }
    }

    pub fn random_track_id(&mut self) -> Uuid {
        let tracks: Vec<Uuid> = self.get_track_ids();
        let track: Uuid = match tracks.choose(&mut self.rand) {
            Some(track) => *track,
            None => Uuid::nil(),
        };
        track.clone()
    }

    pub fn random_connected_track_id(&mut self) -> Uuid {
        let mut tracks: Vec<Uuid> = self.get_track_ids();
        tracks.shuffle(&mut self.rand);
        for track in tracks {
            if self.elements.get(&track).unwrap().is_connected() {
                return track.clone();
            }
        }
        Uuid::nil()
    }

    // returns incoming, if track.0 has incoming edge from left or outgoing edge to right.
    pub fn track_direction(&self, uuid: &Uuid) -> Option<NodeDirection> {
        let track = self.elements.get(uuid).unwrap();
        let track_index = self.node_indices[track.id()];
        if self
            .network
            .neighbors_directed(track_index.0, Outgoing)
            .count()
            > 0
        {
            return Some(NodeDirection::FORWARD);
        } else if self
            .network
            .neighbors_directed(track_index.0, Incoming)
            .count()
            > 0
        {
            return Some(NodeDirection::BACKWARD);
        } else {
            return None;
        }
    }

    pub fn network_as_dot(&self) -> String {
        format!(
            "{}",
            Dot::with_config(&self.network, &[Config::EdgeNoLabel])
        )
    }

    pub fn element_by_index(&self, index: &NodeIndex) -> Uuid {
        for (key, value) in &self.node_indices {
            if value.0 == *index || value.1 == *index {
                return key.clone();
            }
        }
        Uuid::nil()
    }

    pub fn connect_neighbors(&mut self) {
        let elements: Vec<Uuid> = self.elements.keys().cloned().collect();
        for id in &elements {
            let element = self.elements.get(id).unwrap();

            let adjacencies = match element.element_type() {
                RailElementType::TRACK => {
                    let node = self.node_indices.get(id).unwrap();
                    let left = match self.network.neighbors_directed(node.0, Incoming).next() {
                        Some(nb) => AdjacencyRelation::Left(Some(self.element_by_index(&nb))),
                        None => AdjacencyRelation::Left(None),
                    };
                    let right = match self.network.neighbors_directed(node.0, Outgoing).next() {
                        Some(nb) => AdjacencyRelation::Right(Some(self.element_by_index(&nb))),
                        None => AdjacencyRelation::Right(None),
                    };
                    vec![left, right]
                }
                RailElementType::SWITCH => {
                    let node = self.node_indices.get(id).unwrap();
                    let single = match self.network.neighbors_directed(node.0, Incoming).next() {
                        Some(nb) => AdjacencyRelation::Single(Some(self.element_by_index(&nb))),
                        None => AdjacencyRelation::Single(None),
                    };
                    let right = match self.network.neighbors_directed(node.0, Outgoing).next() {
                        Some(nb) => AdjacencyRelation::Right(Some(self.element_by_index(&nb))),
                        None => AdjacencyRelation::Right(None),
                    };
                    vec![single, right]
                }
            };

            let element = self.elements.get_mut(id).unwrap();
            for adjacency in adjacencies {
                element
                    .set_adjacency(adjacency)
                    .expect("adjacency could not be set");
            }
        }
    }
}

impl Default for RailNetwork {
    fn default() -> RailNetwork {
        let track1 =
            Track::new_with_id(Uuid::parse_str("4fdfc20a-dccf-4261-9be8-589d36d1820a").unwrap());
        let track2 =
            Track::new_with_id(Uuid::parse_str("7ef10260-1787-4d8e-9cff-bad7135f81ef").unwrap());
        let track3 =
            Track::new_with_id(Uuid::parse_str("a5a89085-8fd4-4475-8eae-08be78f8e859").unwrap());
        let track4 =
            Track::new_with_id(Uuid::parse_str("f20d5ceb-e3d0-48e0-8a82-91da3add50cb").unwrap());
        let mut rail_network = RailNetwork::new();
        rail_network.add_track(track1);
        rail_network.add_track(track2);
        rail_network.add_track(track3);
        rail_network.add_track(track4);
        rail_network.connect_tracks(track1.id(), track2.id());
        rail_network.connect_tracks(track2.id(), track3.id());
        rail_network.connect_tracks(track3.id(), track4.id());
        rail_network.connect_tracks(track4.id(), track1.id());
        rail_network.connect_neighbors();
        rail_network
    }
}

impl Clone for RailNetwork {
    fn clone(&self) -> Self {
        let cloned = RailNetwork {
            elements: self.elements.clone(),
            network: self.network.clone(),
            node_indices: self.node_indices.clone(),
            rand: self.rand.clone(),
        };
        cloned
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use petgraph::algo::connected_components;

    #[test]
    fn test() {
        use crate::core::position2d::Position2D;
        use crate::rails::track::Track;
        use nalgebra::geometry::Point2;

        let mut rail_network: RailNetwork = RailNetwork::new();
        let mut track = Track::new();
        track.set_position2d(Point2::<f32>::new(1.0, 1.0));
        let switch = Switch::new();
        let track_s2 = Track::new();
        let track_s3 = Track::new();
        rail_network.add_switch(switch);
        rail_network.add_track(track);
        rail_network.add_track(track_s2);
        rail_network.add_track(track_s3);
        rail_network.connect_switch(switch.id(), track.id());
        rail_network.connect_switch(switch.id(), track_s2.id());
        rail_network.connect_switch(switch.id(), track_s3.id());
        assert_eq!(connected_components(&rail_network.network), 2);
        let track2 = Track::new();
        rail_network.add_track(track2);
        rail_network.connect_tracks(track.id(), track2.id());
        let track3 = Track::new();
        rail_network.add_track(track3);
        rail_network.insert_track(track3.id(), track.id());
        assert_eq!(rail_network.elements.len(), 6);
        assert_eq!(
            rail_network
                .elements
                .get(track.id())
                .unwrap()
                .is_connected(),
            true
        );
        println!("{}", rail_network.network_as_dot());
        assert_eq!(connected_components(&rail_network.network), 2);
        assert_eq!(
            rail_network
                .elements
                .get(track.id())
                .unwrap()
                .get_position2d(),
            rail_network
                .elements
                .get(track.id())
                .unwrap()
                .get_position2d()
        );

        let cloned = rail_network.clone();
        assert_eq!(
            rail_network.elements.get(track.id()).unwrap().id(),
            cloned.elements.get(track.id()).unwrap().id()
        );
        assert_eq!(
            rail_network.network.node_count(),
            cloned.network.node_count()
        );
        assert_eq!(
            rail_network.network.edge_count(),
            cloned.network.edge_count()
        );
        assert_eq!(
            rail_network
                .elements
                .get(track.id())
                .unwrap()
                .get_position2d(),
            cloned.elements.get(track.id()).unwrap().get_position2d()
        );
        assert_eq!(
            rail_network
                .elements
                .get(track.id())
                .unwrap()
                .get_position2d(),
            track.get_position2d()
        );
    }

    #[test]
    fn test_default() {
        let rail_network = RailNetwork::default();
        assert_eq!(rail_network.elements.len(), 4);
        assert_eq!(rail_network.node_indices.len(), 4);
        assert_eq!(connected_components(&rail_network.network), 2);
    }
}
