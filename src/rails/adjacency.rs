use std::error::Error;
use std::fmt;
use uuid::Uuid;

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum AdjacencyRelation {
    Single(Option<Uuid>),
    Left(Option<Uuid>),
    Right(Option<Uuid>),
}

impl AdjacencyRelation {
    pub fn get_uuid(&self) -> &Option<Uuid> {
        match self {
            Self::Single(uuid) => uuid,
            Self::Left(uuid) => uuid,
            Self::Right(uuid) => uuid,
        }
    }
}

pub trait Adjacency {
    fn adjacency_list(&self) -> Vec<AdjacencyRelation>;
    fn set_adjacency(&mut self, relation: AdjacencyRelation) -> Result<(), AdjacencyError>;
    fn get_adjacency(
        &self,
        relation: AdjacencyRelation,
    ) -> Result<AdjacencyRelation, AdjacencyError>;
}

#[derive(Debug)]
pub struct AdjacencyError {
    details: String,
}

impl AdjacencyError {
    pub fn new(msg: &str) -> AdjacencyError {
        AdjacencyError {
            details: msg.to_string(),
        }
    }

    pub fn wrong_relation(relation: AdjacencyRelation) -> AdjacencyError {
        AdjacencyError {
            details: format!(
                "{} is a wrong relation.",
                match relation {
                    AdjacencyRelation::Left(_) => "LEFT",
                    AdjacencyRelation::Right(_) => "RIGHT",
                    AdjacencyRelation::Single(_) => "SINGLE",
                }
            ),
        }
    }
}

impl fmt::Display for AdjacencyError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Error for AdjacencyError {
    fn description(&self) -> &str {
        &self.details
    }
}

#[test]
fn test_adjacency_relation() {
    let single_relation: AdjacencyRelation = AdjacencyRelation::Single(None);
    assert_ne!(
        AdjacencyRelation::Single(Some(Uuid::nil())),
        single_relation
    );
    match single_relation {
        AdjacencyRelation::Single(value) => assert_eq!(value, None),
        _ => panic!("relation should be a None Single"),
    };
    let left_relation = AdjacencyRelation::Left(Some(Uuid::nil()));
    match left_relation {
        AdjacencyRelation::Left(Some(value)) => assert_eq!(value, Uuid::nil()),
        _ => panic!("relation should be a Nil Left"),
    };
    assert_eq!(left_relation.get_uuid(), &Some(Uuid::nil()));
    let uuid = Uuid::new_v4();
    let right_relation = AdjacencyRelation::Right(Some(uuid));
    assert_ne!(left_relation, right_relation);
    match right_relation {
        AdjacencyRelation::Right(Some(value)) => assert_eq!(value, uuid),
        _ => panic!("relation should be a Right with uuid"),
    };
    assert_eq!(right_relation.get_uuid(), &Some(uuid));
}

#[test]
fn test_adjacency_error() {
    let error1 = AdjacencyError::new("error");
    assert_eq!(error1.to_string(), format!("error"));
    let error2 = AdjacencyError::wrong_relation(AdjacencyRelation::Left(None));
    assert_eq!(error2.to_string(), format!("LEFT is a wrong relation."));
}
