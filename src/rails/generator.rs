use rand::seq::SliceRandom;
use rand::thread_rng;
use uuid::Uuid;

use nalgebra::geometry::Point2;
use petgraph::algo::connected_components;

use crate::rails::adjacency::AdjacencyRelation;
use crate::rails::rail_element::RailElement;
use crate::rails::rail_network::RailNetwork;
use crate::rails::switch::Switch;
use crate::rails::track::Track;

use crate::rails::physics_generator::PhysicsGenerator;

#[derive(Debug)]
pub struct Generator {
    tracks: u16,
    switches: u16,
    pub rail_network: RailNetwork,
}

impl Generator {
    pub fn new(tracks: u16, switches: u16) -> Generator {
        Generator {
            tracks: tracks,
            switches: switches,
            rail_network: RailNetwork::new(),
        }
    }

    pub fn generate(&mut self) {
        self.generate_switches_with_three_tracks(self.switches);
        self.connect_tracks();
        self.connect_tracks();
        assert!(connected_components(&self.rail_network.network) <= 4);
        self.insert_new_tracks(self.tracks - 3 * self.switches);
        self.init_positions();
        self.normalize_positions(2);
        self.rail_network.connect_neighbors();

        let mut physics_generator = PhysicsGenerator::<f32>::new(&self.rail_network);
        physics_generator.generate();
        physics_generator.update_positions(&mut self.rail_network);
        self.normalize_positions(2);
    }

    pub fn generate_switches_with_three_tracks(&mut self, switches: u16) {
        for _ in 0..switches {
            let switch = Switch::new();
            self.rail_network.add_switch(switch);
            for _ in 0..3 {
                let track = Track::new();
                self.rail_network.add_track(track);
                self.rail_network.connect_switch(switch.id(), track.id());
            }
        }
    }

    pub fn insert_new_tracks(&mut self, tracks: u16) {
        for _ in 0..tracks {
            let track = Track::new();
            self.rail_network.add_track(track);
            let append_to_track = &self.rail_network.random_connected_track_id();
            self.rail_network.insert_track(track.id(), append_to_track);
        }
    }

    pub fn find_disconnected_switch(&self) -> Option<Uuid> {
        let switches = self.rail_network.get_switch_ids();
        for id in switches {
            if !self.rail_network.elements.get(&id).unwrap().is_connected() {
                return Some(id);
            }
        }
        None
    }

    pub fn find_disconnected_track(&self) -> Option<Uuid> {
        let tracks = self.rail_network.get_track_ids();
        for id in tracks {
            if !self.rail_network.elements.get(&id).unwrap().is_connected() {
                return Some(id);
            }
        }
        None
    }

    pub fn connect_elements(&mut self) {
        self.connect_tracks();
        self.connect_tracks();
    }

    pub fn connect_tracks(&mut self) {
        let mut track_keys: Vec<Uuid> = self.rail_network.get_track_ids();
        track_keys.shuffle(&mut thread_rng());
        for track_key1 in &track_keys {
            if !self.rail_network.elements[track_key1].is_connected() {
                for track_key2 in &track_keys {
                    let track1 = &self.rail_network.elements[track_key1];
                    let track2 = &self.rail_network.elements[track_key2];
                    if !track1.is_connected()
                        && !track2.is_connected()
                        && track1.id() != track2.id()
                    {
                        self.rail_network.connect_tracks(&track_key1, &track_key2);
                    }
                }
            }
        }
    }

    pub fn init_positions(&mut self) {
        let radius: f32 = 500.0;
        let element_keys: Vec<Uuid> = self.rail_network.elements.keys().cloned().collect();
        for (i, id) in element_keys.iter().enumerate() {
            let element = self.rail_network.elements.get_mut(id).unwrap();
            let position = element.get_position2d();
            let x = position.x
                + radius
                    * (2.0 * std::f32::consts::PI * i as f32 / element_keys.len() as f32).cos();
            let y = position.y
                + radius
                    * (2.0 * std::f32::consts::PI * i as f32 / element_keys.len() as f32).sin();
            element.set_position2d(Point2::<f32>::new(x, y));
        }
    }

    fn normalize_positions(&mut self, iterations: usize) {
        let track_keys: Vec<Uuid> = self.rail_network.get_track_ids();
        for _ in 0..iterations {
            for (_, id) in track_keys.iter().enumerate() {
                let track = self.rail_network.elements.get_mut(id).unwrap();
                match (
                    track.get_adjacency(AdjacencyRelation::Left(None)),
                    track.get_adjacency(AdjacencyRelation::Right(None)),
                ) {
                    (
                        Ok(AdjacencyRelation::Left(Some(left))),
                        Ok(AdjacencyRelation::Right(Some(right))),
                    ) => {
                        let left_element = self.rail_network.elements.get(&left).unwrap();
                        let right_element = self.rail_network.elements.get(&right).unwrap();
                        let center_x = (left_element.get_position2d().x
                            + right_element.get_position2d().x)
                            / 2.0;
                        let center_y = (left_element.get_position2d().y
                            + right_element.get_position2d().y)
                            / 2.0;
                        let track = self.rail_network.elements.get_mut(id).unwrap();
                        track.set_position2d(Point2::<f32>::new(center_x, center_y));
                    }
                    _ => (),
                }
            }
        }
    }
}

#[test]
fn test() {
    use petgraph::algo::connected_components;
    let tracks: u16 = 15;
    let switches: u16 = 3;

    let mut generator: Generator = Generator::new(tracks, switches);
    assert_eq!(generator.tracks, tracks);
    assert_eq!(generator.switches, switches);

    generator.generate();
    //assert_eq!(connected_components(&generator.rail_network.network), 1);

    assert_eq!(
        generator.rail_network.elements.len(),
        (tracks + switches) as usize
    );
    println!("{:?}", generator.find_disconnected_track());
    match generator.find_disconnected_track() {
        None => assert!(false),
        _ => assert!(true),
    }

    generator.connect_elements();
    assert!(connected_components(&generator.rail_network.network) <= 4);

    assert!(&generator.find_disconnected_switch().is_none());

    println!("{:?}", &generator.rail_network.network_as_dot());
}
