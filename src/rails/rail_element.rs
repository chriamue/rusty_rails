use core::fmt::Debug;
use uuid::Uuid;

use crate::core::position2d::Position2D;
use crate::rails::adjacency::Adjacency;
use crate::rails::direction::Direction;
use crate::rails::state::State;

#[derive(Debug, PartialEq, Clone)]
pub enum RailElementType {
    TRACK,
    SWITCH,
}

pub trait RailElementClone {
    fn clone_box(&self) -> Box<dyn RailElement>;
}

impl<T> RailElementClone for T
where
    T: 'static + RailElement + Clone,
{
    fn clone_box(&self) -> Box<dyn RailElement> {
        Box::new(self.clone())
    }
}

pub trait RailElement:
    Adjacency + Position2D + Direction + State + RailElementClone + Send + Sync
{
    fn id(&self) -> &Uuid;

    fn is_connected(&self) -> bool;

    fn connect_to(&mut self, element: &Uuid);

    fn replace_connection(&mut self, connected: &Uuid, replacement: Option<Uuid>);

    fn element_type(&self) -> RailElementType;
}

impl Clone for Box<dyn RailElement> {
    fn clone(&self) -> Box<dyn RailElement> {
        self.clone_box()
    }
}

impl Debug for dyn RailElement {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "RailElement{{{}}}", self.id())
    }
}
