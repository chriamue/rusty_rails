use rocket::State;
use rocket_contrib::json::Json;
use rocket_contrib::serve::StaticFiles;

#[cfg(feature = "training")]
use crate::ai::trainer::Trainer;

use crate::rails::generator::Generator;
use crate::simulation::agent::Agent;
use crate::simulation::observable::Observable;
use crate::simulation::world::World;

fn log_training(world: &World, _outer: &u32, inner: &u32) {
    if inner % 10 == 0 {
        println!("{:?}", world.value());
    }
}

pub struct WebServer {
    #[cfg(feature = "training")]
    trainer: Trainer,
    #[cfg(not(feature = "training"))]
    world: World,
}

impl WebServer {
    #[cfg(feature = "training")]
    pub fn new(trainer: Trainer) -> WebServer {
        WebServer { trainer: trainer }
    }

    #[cfg(not(feature = "training"))]
    pub fn new(world: World) -> WebServer {
        WebServer { world: world }
    }

    pub fn new_world(&self, world: World) {
        #[cfg(feature = "training")]
        self.trainer.new_world(world);
    }

    fn train(&self, iterations: u32) -> String {
        #[cfg(feature = "training")]
        let world = {
            self.trainer.train(iterations, Box::new(log_training));
            &self.trainer.current.lock().expect("locked current").world
        };
        #[cfg(not(feature = "training"))]
        let world = &self.world;
        format!("{:?}", world)
    }

    fn network_dot(&self) -> String {
        #[cfg(feature = "training")]
        let world = &self.trainer.current.lock().expect("locked current").world;
        #[cfg(not(feature = "training"))]
        let world = &self.world;
        world.rail_network.network_as_dot()
    }

    fn agents(&self) -> Vec<Agent> {
        #[cfg(feature = "training")]
        let world = &self.trainer.current.lock().expect("locked current").world;
        #[cfg(not(feature = "training"))]
        let world = &self.world;
        let agents: Vec<Agent> = world.agents.values().cloned().collect();
        agents
    }
}

#[get("/agents")]
fn api_agents(model: State<WebServer>) -> Json<Vec<Agent>> {
    Json(model.agents())
}

#[get("/new/<tracks>/<switches>/<agents>")]
fn api_new(model: State<WebServer>, tracks: u16, switches: u16, agents: u16) -> String {
    let mut generator = Generator::new(tracks, switches);
    generator.generate();
    println!("{}", generator.rail_network.network_as_dot());
    let mut world = World::new(generator.rail_network);
    for _ in 0..agents {
        world.add_agent(Agent::new());
    }
    let response = format!("{:?}", world);
    model.new_world(world);
    response
}

#[get("/train/<iterations>")]
fn api_train(model: State<WebServer>, iterations: u32) -> String {
    model.train(iterations)
}

#[get("/network.dot")]
fn api_network_dot(model: State<WebServer>) -> String {
    model.network_dot()
}

pub fn start(web_server: WebServer) {
    rocket::ignite()
        .manage(web_server)
        .mount(
            "/api",
            routes![api_agents, api_new, api_train, api_network_dot],
        )
        .mount("/", StaticFiles::from("./react-app/build"))
        .launch();
}
