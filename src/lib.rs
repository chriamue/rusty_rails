//! # Rusty Rails
//!
//! `rusty_rails` is a simulator and framework
//! to train dispatching agents for rail networks.
//!
#![crate_name = "rusty_rails"]
#![feature(decl_macro)]
#[macro_use]
extern crate rocket;

pub mod ai;

pub mod core;
pub mod rails;
pub mod simulation;

#[cfg(not(target_arch = "wasm32"))]
pub mod ui;

#[cfg(any(feature = "vis", target_arch = "wasm32"))]
pub mod vis;

#[cfg(feature = "python")]
pub mod python;

#[cfg(target_arch = "wasm32")]
pub mod wasm;
