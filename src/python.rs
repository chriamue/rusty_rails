use uuid::Uuid;

use pyo3::prelude::*;

use crate::rails::state::StateType;
use crate::simulation::motion::MotionType;
use crate::simulation::observable::Observable;

#[pyclass]
#[derive(Debug, Clone)]
pub struct World {
    num_tracks: u16,
    num_switches: u16,
    num_agents: u16,
    inner: crate::simulation::world::World,
}

#[pymethods]
impl World {
    #[new]
    fn new(tracks: u16, switches: u16, agents: u16) -> Self {
        use crate::rails::generator::Generator;
        use crate::simulation::agent::Agent;

        let mut generator: Generator = Generator::new(tracks, switches);
        generator.generate();
        let mut world = crate::simulation::world::World::new(generator.rail_network);

        for _ in 0..agents {
            world.add_agent(Agent::new());
        }
        World {
            num_tracks: tracks,
            num_switches: switches,
            num_agents: agents,
            inner: world,
        }
    }

    fn dot(&self) -> PyResult<String> {
        Ok(self.inner.rail_network.network_as_dot())
    }

    fn update(&mut self) -> PyResult<()> {
        Ok(self.inner.update())
    }

    fn agents(&self) -> PyResult<Vec<String>> {
        Ok(self
            .inner
            .agent_ids
            .to_vec()
            .into_iter()
            .map(|v| v.to_string())
            .collect())
    }

    fn flat(&self) -> PyResult<Vec<f64>> {
        Ok(self.inner.flat())
    }

    fn value(&self) -> PyResult<f64> {
        Ok(self.inner.value())
    }

    fn update_motion(&mut self, motion: usize) -> PyResult<()> {
        let motion = MotionType::from_usize(motion);
        Ok(self.inner.order.update_motion(&motion))
    }

    fn update_state(&mut self, state: usize) -> PyResult<()> {
        let state = StateType::from_usize(state);
        Ok(self.inner.order.update_state(state))
    }

    fn update_order(&mut self, a: usize) {
        let motions = 3 * self.inner.agent_ids.len();
        if a >= motions {
            let state = StateType::from_usize(a - motions);
            self.inner.order.update_state(state);
        } else {
            let motion = MotionType::from_usize(a);
            self.inner.order.update_motion(&motion);
        }
        self.inner.update();
    }

    fn collision(&self) -> bool {
        self.inner.collision()
    }

    fn tracks_count(&self) -> u16 {
        self.num_tracks
    }

    fn switches_count(&self) -> u16 {
        self.num_switches
    }

    fn agents_count(&self) -> u16 {
        self.num_agents
    }

    fn clone(&self) -> Self {
        World {
            num_tracks: self.num_tracks,
            num_switches: self.num_switches,
            num_agents: self.num_agents,
            inner: self.inner.clone(),
        }
    }

    fn agent_position(&self, agent_id: String) -> (String, String) {
        let uuid = Uuid::parse_str(&agent_id).unwrap();
        match self.inner.agents.get(&uuid) {
            Some(agent) => (agent.position.0.to_string(), agent.position.1.to_string()),
            _ => (Uuid::nil().to_string(), Uuid::nil().to_string()),
        }
    }
}

/// Rusty Rails is a simulator and framework to train dispatching agents for rail networks.
#[pymodule]
fn rusty_rails(_py: Python, m: &PyModule) -> PyResult<()> {
    #[pyfn(m, "generate_rail_network")]
    pub fn py_generate_rail_network(_py: Python, tracks: u16, switches: u16) -> PyResult<String> {
        use crate::rails::generator::Generator;
        let mut generator: Generator = Generator::new(tracks, switches);
        generator.generate();
        Ok(generator.rail_network.network_as_dot())
    }

    m.add_class::<World>()?;

    Ok(())
}

#[test]
fn test() {
    let tracks = 42;
    let switches = 3;
    let agents = 2;
    let world = World::new(tracks, switches, agents);

    assert_eq!(world.agents_count(), 2);
    assert_eq!(world.agents().unwrap().len(), 2);
}
