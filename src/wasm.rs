use kiss3d::light::Light;
use kiss3d::window::{State, Window};
use wasm_bindgen::prelude::*;

use crate::ai::default_agent::DefaultAgent;
use crate::ai::actor::Actor;
use crate::ai::observer::Observer;
use crate::rails::generator::Generator;
use crate::rails::state::StateType;
use crate::simulation::motion::MotionType;
use crate::simulation::agent::Agent;
use crate::simulation::world::World;
use crate::simulation::observable::Observable;
use crate::vis::Vis;
use crate::vis::world_vis::WorldVis;

#[wasm_bindgen(js_name = generateRailNetwork)]
pub fn generate_rail_network(tracks: u16, switches: u16) -> String {
    use crate::rails::generator::Generator;
    let mut generator: Generator = Generator::new(tracks, switches);
    generator.generate();
    generator.rail_network.network_as_dot()
}

struct AppState {
    pub default_agent: DefaultAgent,
    pub world: World,
    pub world_vis: WorldVis
}

impl AppState {

    pub fn update(&mut self) {
        self.update_observation();
        let action = self.default_agent.action();
        self.update_state(action);
    }

    pub fn update_observation(&mut self) {
        self.default_agent.observe(self.world.flat());
    }

    pub fn update_state(&mut self, a: usize) {
        let motions = 3 * self.world.agent_ids.len();
        if a >= motions {
            let state = StateType::from_usize(a - motions);
            self.world.order.update_state(state);
        } else {
            let motion = MotionType::from_usize(a);
            self.world.order.update_motion(&motion);
        }
        self.world.update();
    }
}

impl State for AppState {
    fn step(&mut self, window: &mut Window) {
        self.update();
        self.world_vis.update_vis(&self.world, window);
    }
}

#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    let mut window = Window::new_with_size("Rusty Rails", 1200, 800);

    window.set_light(Light::StickToCamera);

    let tracks = 100;
    let switches = 5;
    let mut generator = Generator::new(tracks, switches);
    generator.generate();

    println!("{}", generator.rail_network.network_as_dot());

    let mut world = World::new(generator.rail_network);

    let agent1 = Agent::new();
    let agent2 = Agent::new();
    world.add_agent(agent1);
    world.add_agent(agent2);
    let default_agent = DefaultAgent::new(2, switches.into());

    let world_vis = WorldVis::new(&world, &mut window);

    let state = AppState {
        default_agent: default_agent, world: world, world_vis: world_vis };
    window.render_loop(state);
    Ok(())
}
