use nalgebra::geometry::Point2;

pub trait Position2D {
    fn set_position2d(&mut self, position: Point2<f32>);

    fn get_position2d(&self) -> Point2<f32>;
}
