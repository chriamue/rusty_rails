use std::collections::HashMap;
use uuid::Uuid;

use kiss3d::nalgebra::{Point3, Translation3};
use kiss3d::scene::SceneNode;
use kiss3d::window::Window;

use crate::rails::rail_element::RailElementType;
use crate::rails::rail_network::RailNetwork;
use crate::rails::state::StateType;
use crate::simulation::world::World;
use crate::vis::Vis;

pub struct NetworkVis {
    scene_nodes: HashMap<Uuid, SceneNode>,
}

impl NetworkVis {
    pub fn new(rail_network: &RailNetwork, window: &mut Window) -> NetworkVis {
        let mut network_vis = NetworkVis {
            scene_nodes: HashMap::new(),
        };
        network_vis.show_elements(rail_network, window);
        network_vis.show_connections(rail_network, window);
        network_vis
    }

    fn show_elements(&mut self, rail_network: &RailNetwork, window: &mut Window) {
        for id in rail_network.get_element_ids() {
            let element = rail_network.elements.get(&id);
            let position = element.unwrap().get_position2d();
            let mut sphere = window.add_sphere(5.0);
            match element.unwrap().element_type() {
                RailElementType::TRACK => sphere.set_color(1.0, 0.0, 0.5),
                RailElementType::SWITCH => sphere.set_color(0.5, 0.0, 1.0),
            }
            sphere.append_translation(&Translation3::new(position.x, position.y, 0.0));
            self.scene_nodes.insert(id, sphere);
        }
    }

    fn show_switch_state(&mut self, rail_network: &RailNetwork) {
        for id in rail_network.get_switch_ids() {
            let element = rail_network.elements.get(&id).unwrap();
            let sphere = self.scene_nodes.get_mut(&id).unwrap();
            match element.get_state() {
                Some(StateType::Left) => sphere.set_color(0.5, 0.0, 1.0),
                Some(StateType::Right) => sphere.set_color(0.3, 0.3, 1.0),
                _ => (),
            };
        }
    }

    fn show_connections(&self, rail_network: &RailNetwork, window: &mut Window) {
        for id in rail_network.elements.keys() {
            match rail_network.elements.get(id) {
                Some(element) => {
                    let position = element.get_position2d();
                    for adjacency_relation in element.adjacency_list() {
                        match adjacency_relation.get_uuid() {
                            Some(uuid) => match rail_network.elements.get(uuid) {
                                Some(adjacent) => {
                                    let adjacent_position = adjacent.get_position2d();
                                    window.draw_line(
                                        &Point3::new(position.x, position.y, 0.0),
                                        &Point3::new(adjacent_position.x, adjacent_position.y, 0.0),
                                        &Point3::new(1.0, 0.0, 0.5),
                                    )
                                }
                                None => (),
                            },
                            None => (),
                        };
                    }
                }
                _ => (),
            };
        }
    }
}

impl Vis for NetworkVis {
    fn update_vis(&mut self, world: &World, window: &mut Window) {
        self.show_switch_state(&world.rail_network);
        self.show_connections(&world.rail_network, window);
    }
}
