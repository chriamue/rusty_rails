//! # Vis
//!
//! Using Kiss3d to visualize rail network and statistics.
//!
use crate::simulation::world::World;
use kiss3d::window::Window;

pub trait Vis {
    fn update_vis(&mut self, world: &World, window: &mut Window);
}

pub mod agent_vis;
pub mod network_vis;
pub mod observer_vis;
pub mod world_vis;
