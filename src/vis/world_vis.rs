use kiss3d::window::Window;
use std::collections::HashMap;
use uuid::Uuid;

use crate::simulation::world::World;
use crate::vis::agent_vis::AgentVis;
use crate::vis::network_vis::NetworkVis;
use crate::vis::observer_vis::ObserverVis;
use crate::vis::Vis;

pub struct WorldVis {
    pub agents_vis: HashMap<Uuid, AgentVis>,
    pub network_vis: NetworkVis,
    pub observer_vis: ObserverVis,
}

impl WorldVis {
    pub fn new(world: &World, window: &mut Window) -> WorldVis {
        let network_vis = NetworkVis::new(&world.rail_network, window);
        let observer_vis = ObserverVis::new();
        let mut world_vis = WorldVis {
            agents_vis: HashMap::new(),
            network_vis: network_vis,
            observer_vis: observer_vis,
        };
        for (id, agent) in &world.agents {
            let agent_vis = AgentVis::new(&agent.id, window);
            world_vis.agents_vis.insert(*id, agent_vis);
        }
        world_vis
    }
}

impl Vis for WorldVis {
    fn update_vis(&mut self, world: &World, window: &mut Window) {
        self.network_vis.update_vis(world, window);
        for agent in self.agents_vis.values_mut() {
            agent.update_vis(world, window);
        }
        self.observer_vis.update_vis(world, window);
    }
}

#[test]
#[ignore]
fn test() {
    let mut window = Window::new_with_size("World Vis Test", 800, 600);
    let world = World::default();
    let mut world_vis = WorldVis::new(&world, &mut window);
    world_vis.update_vis(&world, &mut window);
    assert_eq!(world_vis.agents_vis.len(), 2);
    window.close();
}
