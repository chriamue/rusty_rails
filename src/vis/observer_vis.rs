use kiss3d::nalgebra::{Point2, Point3};
use kiss3d::text::Font;
use kiss3d::window::Window;

use crate::simulation::observable::Observable;
use crate::simulation::world::World;
use crate::vis::Vis;

pub struct ObserverVis {}

impl ObserverVis {
    pub fn new() -> ObserverVis {
        ObserverVis {}
    }
}

impl Vis for ObserverVis {
    fn update_vis(&mut self, world: &World, window: &mut Window) {
        let font = Font::default();

        window.draw_text(
            &format!("Observer {} {}", world.collision(), world.value()),
            &Point2::new(200.0, 500.0),
            50.0,
            &font,
            &Point3::new(0.0, 1.0, 1.0),
        );

        for (i, order) in world.order.motion.iter().enumerate() {
            window.draw_text(
                &format!("Order {:?}", order),
                &Point2::new(200.0, 600.0 + i as f32 * 50.0),
                50.0,
                &font,
                &Point3::new(0.0, 1.0, 1.0),
            );
        }
    }
}
