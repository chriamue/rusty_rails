use uuid::Uuid;

use kiss3d::nalgebra::Translation3;
use kiss3d::nalgebra::{Point2, Point3};
use kiss3d::scene::SceneNode;
use kiss3d::text::Font;
use kiss3d::window::Window;

use crate::core::position2d::Position2D;
use crate::simulation::world::World;
use crate::vis::Vis;

pub struct AgentVis {
    scene_node: SceneNode,
    id: Uuid,
}

impl AgentVis {
    pub fn new(id: &Uuid, window: &mut Window) -> AgentVis {
        let mut sphere = window.add_sphere(10.0);
        sphere.set_color(0.0, 0.0, 1.0);
        AgentVis {
            scene_node: sphere,
            id: id.clone(),
        }
    }
}

impl Vis for AgentVis {
    fn update_vis(&mut self, world: &World, window: &mut Window) {
        let agent = world.agents.get(&self.id).unwrap();
        self.scene_node.set_local_translation(Translation3::new(
            agent.get_position2d().x,
            agent.get_position2d().y,
            0.0,
        ));

        let font = Font::default();
        window.draw_text(
            &format!(
                "Agent {} {} {}",
                agent.id.to_string(),
                agent.distance,
                agent.points
            ),
            &Point2::new(200.0, 100.0 * agent.index as f32),
            120.0,
            &font,
            &Point3::new(0.0, 1.0, 1.0),
        );
    }
}

#[test]
#[ignore]
fn test() {
    use crate::simulation::agent::Agent;

    let mut window = Window::new_with_size("Agent Vis Test", 800, 600);
    let agent = Agent::default();
    let agent_vis = AgentVis::new(&agent.id, &mut window);
    assert!(agent_vis.scene_node.is_visible());
    window.close();
}
