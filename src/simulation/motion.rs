use uuid::Uuid;

use crate::simulation::world::World;

#[derive(Debug, Eq, PartialEq, Hash, Clone, Copy)]
pub enum MotionType {
    Stop(usize),
    Forward(usize),
    Backward(usize),
}

impl MotionType {
    pub fn from_usize(i: usize) -> MotionType {
        match i % 3 {
            0 => MotionType::Stop(i / 3),
            1 => MotionType::Forward(i / 3),
            2 => MotionType::Backward(i / 3),
            _ => panic!("Unknown motion {}!", i),
        }
    }
}

pub trait Motion {
    fn update_motion(&mut self);
    fn move_forward(&mut self, agent_id: &Uuid);
    fn move_backward(&mut self, agent_id: &Uuid);
}

impl Motion for World {
    fn update_motion(&mut self) {
        for motion in self.order.motion.to_vec() {
            match motion {
                MotionType::Stop(_) => (),
                MotionType::Forward(e) => {
                    let uuid: &Uuid = &self.order.agents[e].clone();
                    self.move_forward(uuid);
                }
                MotionType::Backward(e) => {
                    let uuid: &Uuid = &self.order.agents[e].clone();
                    self.move_backward(uuid);
                }
            }
        }
    }

    fn move_forward(&mut self, agent_id: &Uuid) {
        let mut agent = self.agents.get_mut(agent_id).unwrap();
        agent.points += 1;
        let position = agent.position;
        let current = agent.position.1;

        match self.rail_network.elements.get(&position.1) {
            Some(element) => agent.position = (current, element.rear((position.1, position.0))),
            _ => {
                let random_track_id = &self.rail_network.random_track_id();
                self.new_start_position(agent_id, random_track_id)
            }
        };
    }

    fn move_backward(&mut self, agent_id: &Uuid) {
        let mut agent = self.agents.get_mut(agent_id).unwrap();
        agent.points += 1;
        let position = agent.position;
        let front = agent.position.0;

        match self.rail_network.elements.get(&front) {
            Some(element) => agent.position = (element.rear(position), front),
            _ => {
                let random_track_id = &self.rail_network.random_track_id();
                self.new_start_position(agent_id, random_track_id)
            }
        };
    }
}

#[test]
fn test_motion_type() {
    assert_eq!(MotionType::from_usize(0), MotionType::Stop(0));
    assert_eq!(MotionType::from_usize(4), MotionType::Forward(1));
    assert_eq!(MotionType::from_usize(8), MotionType::Backward(2));
}

#[test]
fn test_motion() {
    let mut world = World::default();
    let agent_id: Uuid = world.agent_ids[0];
    let position = world.agents.get(&agent_id).unwrap().position;

    let motion_type = MotionType::Forward(0);
    world.order.update_motion(&motion_type);
    world.update();

    assert_ne!(position, world.agents.get(&agent_id).unwrap().position);
    world.update();

    assert_eq!(world.collision(), true);
    world.update();
    world.update();
    assert_eq!(position, world.agents.get(&agent_id).unwrap().position);

    world.move_forward(&agent_id);
    world.move_backward(&agent_id);
    assert_eq!(position, world.agents.get(&agent_id).unwrap().position);
}
