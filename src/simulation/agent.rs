use chrono::prelude::*;
use serde::Serialize;
use std::default::Default;
use uuid::Uuid;

use crate::core::position2d::Position2D;
use crate::rails::direction::DirectionType;

use nalgebra::geometry::Point2;

#[derive(Debug, Clone, Serialize)]
pub struct Agent {
    pub id: Uuid,
    pub index: usize,
    pub position: DirectionType,
    pub destination: Uuid,
    pub path: Vec<Uuid>,
    pub born: i64,
    pub last_distance: u32,
    pub distance: u32,
    pub points: u32,
    pub position2d: Point2<f32>,
}

impl Agent {
    pub fn new() -> Agent {
        Agent {
            id: Uuid::new_v4(),
            index: 0,
            position: (Uuid::nil(), Uuid::nil()),
            destination: Uuid::nil(),
            path: vec![],
            born: Utc::now().timestamp(),
            last_distance: 0,
            distance: 0,
            points: 0,
            position2d: Point2::<f32>::new(0.0, 0.0),
        }
    }

    pub fn set_distance(&mut self, distance: u32) {
        self.last_distance = self.distance;
        self.distance = distance;
    }
}

impl Position2D for Agent {
    fn set_position2d(&mut self, position: Point2<f32>) {
        self.position2d = position;
    }

    fn get_position2d(&self) -> Point2<f32> {
        self.position2d
    }
}

impl Default for Agent {
    fn default() -> Agent {
        Agent::new()
    }
}

#[test]
fn test() {
    let mut agent = Agent::new();
    assert_eq!(agent.position.0, Uuid::nil());
    assert_eq!(agent.destination, Uuid::nil());
    assert_eq!(agent.distance, 0);
    assert_eq!(agent.path.len(), 0);

    let last_distance = 5;
    let distance = 7;
    agent.set_distance(last_distance);
    agent.set_distance(distance);
    assert_eq!(agent.last_distance, last_distance);
}
