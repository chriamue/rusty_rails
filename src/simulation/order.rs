use crate::rails::state::StateType;
use crate::simulation::motion::MotionType;
use uuid::Uuid;

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct Order {
    pub agents: Vec<Uuid>,
    pub motion: Vec<MotionType>,
    pub switches: Vec<Uuid>,
    pub states: Vec<StateType>,
}

impl Order {
    pub fn new(agents: Vec<Uuid>, switches: Vec<Uuid>) -> Order {
        let mut motion = Vec::<MotionType>::new();
        for i in 0..agents.len() {
            motion.push(MotionType::Stop(i));
        }
        let mut states = Vec::<StateType>::new();
        for _ in 0..switches.len() {
            states.push(StateType::Left);
        }
        Order {
            agents: agents.to_vec(),
            motion: motion,
            switches: switches.to_vec(),
            states: states,
        }
    }

    pub fn update_motion(&mut self, motion: &MotionType) {
        match motion {
            MotionType::Stop(e) => self.motion[*e] = MotionType::Stop(*e),
            MotionType::Forward(e) => self.motion[*e] = MotionType::Forward(*e),
            MotionType::Backward(e) => self.motion[*e] = MotionType::Backward(*e),
        }
    }

    pub fn update_state(&mut self, state: (StateType, usize)) {
        self.states[state.1] = state.0;
    }
}

#[test]
fn test() {
    let agents = vec![Uuid::new_v4(), Uuid::new_v4(), Uuid::new_v4()];
    let switches = vec![Uuid::new_v4(), Uuid::new_v4()];
    let mut order = Order::new(agents, switches);

    assert_eq!(order.agents.len(), 3);
    assert_eq!(order.motion.len(), 3);
    let motion = MotionType::Forward(2);
    order.update_motion(&motion);
    assert_eq!(order.motion[2], MotionType::Forward(2));
    assert_eq!(order.motion.len(), 3);

    assert_eq!(order.switches.len(), 2);
    assert_eq!(order.states.len(), 2);
    let state = StateType::Right;
    order.update_state((state, 1));
    assert_eq!(order.states[1], StateType::Right);
}
