use chrono::prelude::*;
use instant::Instant;
use std::collections::HashMap;
use uuid::Uuid;

use crate::core::position2d::Position2D;
use crate::rails::adjacency::AdjacencyRelation;
use crate::rails::distance::Distance;
use crate::rails::path_search::PathSearch;
use crate::rails::rail_network::RailNetwork;
use crate::simulation::agent::Agent;
use crate::simulation::motion::{Motion, MotionType};
use crate::simulation::order::Order;

const DESTINATION_REACHED_POINTS: u32 = 100;

#[derive(Debug, Clone)]
pub struct World {
    pub genesis: Instant,
    pub agent_ids: Vec<Uuid>,
    pub agents: HashMap<Uuid, Agent>,
    pub rail_network: RailNetwork,
    pub order: Order,
}

impl World {
    pub fn new(rail_network: RailNetwork) -> World {
        let switch_ids = rail_network.get_switch_ids().to_vec();
        World {
            genesis: Instant::now(),
            agent_ids: Vec::new(),
            agents: HashMap::new(),
            rail_network: rail_network,
            order: Order::new(vec![], switch_ids),
        }
    }

    pub fn init(&mut self) {
        self.genesis = Instant::now();
        for id in &self.agent_ids {
            let mut agent = self.agents.get_mut(id).unwrap();
            agent.destination = self.rail_network.random_track_id();
            agent.born = Utc::now().timestamp();
            agent.distance = 0;
            agent.points = 0;
        }
        self.update_start_positions();
    }

    pub fn add_agent(&mut self, mut agent: Agent) {
        self.agent_ids.push(agent.id);
        agent.index = self.agent_ids.len();
        self.order.agents.push(agent.id);
        self.order
            .motion
            .push(MotionType::Stop(self.order.agents.len() - 1));
        self.agents.insert(agent.id, agent);
    }

    pub fn update(&mut self) {
        self.update_states();
        self.update_motion();
        self.update_distance();
        self.update_destination();
        self.update_start_positions();
        self.update_positions();
    }

    pub fn update_states(&mut self) {
        for (i, state) in self.order.states.iter().enumerate() {
            let switch = self
                .rail_network
                .elements
                .get_mut(&self.order.switches[i])
                .unwrap();
            switch.change_state(*state);
        }
    }

    pub fn update_distance(&mut self) {
        for id in &self.agent_ids {
            let from = self.agents.get(id).unwrap().position.0;
            let to = self.agents.get(id).unwrap().destination;
            let distance = self.rail_network.distance(&from, &to);
            match distance {
                Some(distance) => {
                    self.agents.get_mut(id).unwrap().set_distance(distance);
                }
                None => {
                    self.agents.get_mut(id).unwrap().destination =
                        self.rail_network.random_track_id();
                }
            }
        }
    }

    pub fn update_destination(&mut self) {
        for id in &self.agent_ids {
            let mut agent = self.agents.get_mut(id).unwrap();
            if agent.destination == Uuid::nil() || agent.destination == agent.position.0 {
                agent.points += DESTINATION_REACHED_POINTS;
                agent.destination = self.rail_network.random_track_id();
                match self
                    .rail_network
                    .shortest_path(&agent.position.0, &agent.destination)
                {
                    Some(path) => agent.path = path,
                    _ => agent.path = Vec::new(),
                };
            }
        }
    }

    pub fn agent_collides(&self, agent_id: &Uuid) -> bool {
        let position = self.agents.get(agent_id).unwrap().position;
        for (id, agent) in &self.agents {
            if *id != *agent_id {
                if agent.position.0 == position.0 && agent.position.0 != Uuid::nil() {
                    return true;
                }
            }
        }
        return false;
    }

    pub fn collision(&self) -> bool {
        for agent in &self.agent_ids {
            if self.agent_collides(agent) {
                return true;
            }
        }
        false
    }

    pub fn update_start_positions(&mut self) {
        for id in &self.agent_ids.clone() {
            let agent = self.agents.get(id).unwrap();
            if agent.position.0 == Uuid::nil() {
                let element = self.rail_network.random_track_id();
                for _ in 0..5 {
                    if self.agent_collides(id) {
                        self.new_start_position(id, &element);
                    }
                }
            }
        }
    }

    pub fn new_start_position(&mut self, agent_id: &Uuid, element_id: &Uuid) {
        println!("new start for {:?} at {:?}", agent_id, element_id);
        let mut agent = self.agents.get_mut(agent_id).unwrap();
        let track = self.rail_network.elements.get(&element_id).unwrap();
        match track.get_adjacency(AdjacencyRelation::Left(None)) {
            Ok(AdjacencyRelation::Left(Some(front))) => agent.position = (*element_id, front),
            _ => agent.position = (Uuid::nil(), Uuid::nil()),
        };
    }

    pub fn update_positions(&mut self) {
        for id in &self.agent_ids.clone() {
            let position = self.agents.get(id).unwrap().position;
            match self.rail_network.elements.get(&position.0) {
                Some(element) => {
                    let agent = self.agents.get_mut(id).unwrap();
                    agent.set_position2d(element.get_position2d())
                }
                _ => (),
            }
        }
    }
}

impl Default for World {
    fn default() -> World {
        let rail_network = RailNetwork::default();
        let element_ids = rail_network.get_track_ids();
        let mut agent1 = Agent::default();
        let mut agent2 = Agent::default();
        agent1.position = (element_ids[0], element_ids[1]);
        agent2.position = (element_ids[2], element_ids[3]);
        let mut world = World::new(rail_network);
        world.add_agent(agent1);
        world.add_agent(agent2);
        world
    }
}

#[test]
fn test() {
    use crate::rails::generator::Generator;

    let mut generator = Generator::new(100, 5);
    generator.generate();
    generator.connect_elements();
    let mut world = World::new(generator.rail_network);

    let agent1 = Agent::new();
    let agent2 = Agent::new();
    world.add_agent(agent1);
    world.add_agent(agent2);

    assert_eq!(world.agents.len(), 2);
}

#[test]
fn test_default() {
    let world = World::default();
    assert_eq!(world.agents.len(), 2);
}

#[test]
fn test_collission() {
    use crate::rails::generator::Generator;

    let mut generator = Generator::new(100, 5);
    generator.generate();
    generator.connect_elements();
    let mut world = World::new(generator.rail_network);

    let mut agent1 = Agent::new();
    let mut agent2 = Agent::new();

    let position = Uuid::new_v4();
    agent1.position.0 = position;
    agent2.position.0 = position;

    world.add_agent(agent1);
    world.add_agent(agent2);

    assert_eq!(world.collision(), true);
}
