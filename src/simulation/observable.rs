use chrono::prelude::Utc;

use crate::rails::state::StateType;
use crate::simulation::agent::Agent;
use crate::simulation::world::World;

pub trait Observable {
    fn value(&self) -> f64;
    fn flat(&self) -> Vec<f64>;
}

impl Observable for Agent {
    fn value(&self) -> f64 {
        let mut value: f64 = 0.0;
        // value -= self.distance as f64;
        value += (self.last_distance as f64 - self.distance as f64) * 10.0;
        // value += (Utc::now().timestamp() - self.born) as f64;
        value += self.points as f64;
        value
    }

    fn flat(&self) -> Vec<f64> {
        let mut flat: Vec<f64> = Vec::new();
        flat.push(self.distance as f64);
        flat.push(self.last_distance as f64 - self.distance as f64);
        flat
    }
}

impl Observable for World {
    fn value(&self) -> f64 {
        let mut value: f64 = 0.0;

        for (_, agent) in &self.agents {
            value += agent.value();
            value += self.rail_network.switches_set_right_in_path(&agent.path) as f64;
        }
        value
    }
    fn flat(&self) -> Vec<f64> {
        let mut flat: Vec<f64> = Vec::new();
        let switch_ids = self.rail_network.get_switch_ids();
        for switch_id in switch_ids {
            let switch = self.rail_network.elements.get(&switch_id).unwrap();
            match switch.get_state() {
                Some(StateType::Left) => flat.push(1.0),
                _ => flat.push(0.0),
            }
        }
        for (_, agent) in &self.agents {
            flat.append(&mut agent.flat());
        }
        flat
    }
}

#[test]
fn test_agent() {
    let agent = Agent::new();
    assert_eq!(agent.value(), 0.0);
}

#[test]
fn test_world() {
    let world = World::default();
    assert_eq!(world.value(), 0.0);
}
