use rusty_rails::ai::trainer::Trainer;
use rusty_rails::rails::generator::Generator;
use rusty_rails::simulation::agent::Agent;
use rusty_rails::simulation::world::World;
use rusty_rails::ui::web_server::{start, WebServer};

fn main() {
    let mut generator = Generator::new(100, 5);
    generator.generate();
    println!("{}", generator.rail_network.network_as_dot());
    let mut world = World::new(generator.rail_network);

    let agent1 = Agent::new();
    let agent2 = Agent::new();
    world.add_agent(agent1);
    world.add_agent(agent2);

    let trainer = Trainer::new(world);
    let server = WebServer::new(trainer);
    start(server);
}
