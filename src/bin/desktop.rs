use kiss3d::camera::FirstPerson;
use kiss3d::light::Light;
use kiss3d::nalgebra::Point3;
use kiss3d::window::Window;

use rusty_rails::ai::trainer::{Trainer, TrainingCallback};
use rusty_rails::rails::generator::Generator;
use rusty_rails::simulation::agent::Agent;
use rusty_rails::simulation::observable::Observable;
use rusty_rails::simulation::world::World;
use rusty_rails::vis::world_vis::WorldVis;
use rusty_rails::vis::Vis;

fn main() {
    let mut window = Window::new_with_size("Rusty Rails", 1200, 800);

    window.set_light(Light::StickToCamera);

    let eye = Point3::new(10.0f32, 10.0, 500.0);
    let at = Point3::origin();
    let mut first_person = FirstPerson::new(eye, at);

    let mut generator = Generator::new(500, 8);
    generator.generate();

    let mut world = World::new(generator.rail_network);

    let agent1 = Agent::new();
    let agent2 = Agent::new();
    let _agent3 = Agent::new();
    world.add_agent(agent1);
    world.add_agent(agent2);
    // world.add_agent(agent3);

    let mut world_vis = WorldVis::new(&world, &mut window);

    let trainer = Trainer::new(world);

    let callback: TrainingCallback = Box::new(move |world, _outer, inner| {
        if inner % 1 == 0 {
            world_vis.update_vis(world, &mut window);
            window.render_with_camera(&mut first_person);
            println!("{:?}", world.value());
        }
    });

    trainer.train(50, callback);
}
