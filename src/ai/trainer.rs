use rand::{rngs::StdRng, SeedableRng};
use rsrl::{
    control::td::QLearning,
    domains::Domain,
    fa::linear::{
        basis::{Combinators, Fourier},
        optim::SGD,
        LFA,
    },
    make_shared,
    policies::{Greedy, Policy},
    spaces::Space,
    Handler,
};
use std::sync::Mutex;

use crate::ai::ai_domain::AiDomain;
use crate::simulation::world::World;

pub type TrainingCallback = Box<dyn FnMut(&World, &u32, &u32)>;

#[derive(Debug)]
pub struct Trainer {
    pub env: Mutex<AiDomain>,
    pub current: Mutex<AiDomain>,
    pub max_loops: u32,
}

impl Trainer {
    pub fn new(world: World) -> Trainer {
        let env = AiDomain::new(world);
        Trainer {
            current: Mutex::new(env.clone()),
            env: Mutex::new(env),
            max_loops: 100000
        }
    }

    pub fn new_world(&self, world: World) {
        let mut env = self.env.lock().expect("locked env");
        *env = AiDomain::new(world);
    }

    pub fn train(&self, iterations: u32, mut callback: TrainingCallback) {
        let env = self.env.lock().expect("locked env");
        let n_actions = env.action_space().card().into();

        let mut rng = StdRng::seed_from_u64(0);
        let (mut ql, policy) = {
            let basis = Fourier::from_space(5, env.state_space()).with_bias();
            let q_func = make_shared(LFA::vector(basis, SGD(0.01), n_actions));
            let policy = Greedy::new(q_func.clone());

            (QLearning { q_func, gamma: 0.8 }, policy)
        };

        for e in 0..iterations {
            // Episode loop:
            let mut j = 0;
            let mut env: AiDomain = env.clone();
            env.world.init();
            let mut action = policy.sample(&mut rng, env.emit().state());

            for i in 0..self.max_loops {
                // Trajectory loop:
                j = i;

                let t = env.transition(action);

                ql.handle(&t).ok();
                action = policy.sample(&mut rng, t.to.state());

                callback(&env.world, &e, &i);
                if t.terminated() {
                    println!("{:?}", action);
                    break;
                }
            }
            let mut current = self.current.lock().expect("locked current");
            *current = env.clone();

            println!("Batch {}: {} steps...", e + 1, j + 1);
        }
    }
}
