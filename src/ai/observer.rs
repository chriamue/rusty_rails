pub trait Observer {
    fn observe(&mut self, observation: Vec<f64>);
}
