use instant::{Duration, Instant};

use rsrl::{
    domains::{Domain, Observation, Reward},
    spaces::{discrete::Ordinal, real::Interval, ProductSpace},
};

use crate::rails::generator::Generator;
use crate::rails::state::StateType;
use crate::simulation::agent::Agent;
use crate::simulation::motion::MotionType;
use crate::simulation::observable::Observable;
use crate::simulation::world::World;

const MAX_DURATION_SEC: Duration = Duration::from_secs(60);

#[derive(Debug, Clone)]
pub struct AiDomain {
    pub world: World,
    pub last_observation: Vec<f64>,
}

impl AiDomain {
    pub fn new(world: World) -> AiDomain {
        AiDomain {
            world: world,
            last_observation: vec![],
        }
    }

    fn update_state(&mut self, a: usize) {
        let motions = 3 * self.world.agent_ids.len();
        if a >= motions {
            let state = StateType::from_usize(a - motions);
            self.world.order.update_state(state);
        } else {
            let motion = MotionType::from_usize(a);
            self.world.order.update_motion(&motion);
        }
        self.world.update();
    }
}

impl Default for AiDomain {
    fn default() -> AiDomain {
        let mut generator = Generator::new(100, 5);
        generator.generate();
        generator.connect_elements();
        let mut world = World::new(generator.rail_network);

        let agent1 = Agent::new();
        let agent2 = Agent::new();
        world.add_agent(agent1);
        world.add_agent(agent2);
        AiDomain::new(world)
    }
}

impl Domain for AiDomain {
    type StateSpace = ProductSpace<Interval>;
    type ActionSpace = Ordinal;

    fn emit(&self) -> Observation<Vec<f64>> {
        if Instant::now() - self.world.genesis > MAX_DURATION_SEC {
            return Observation::Terminal(self.world.flat());
        }

        match self.world.collision() {
            true => Observation::Terminal(self.world.flat()),
            false => Observation::Full(self.world.flat()),
        }
    }

    fn step(&mut self, action: &usize) -> (Observation<Vec<f64>>, Reward) {
        self.update_state(*action);

        let to = self.emit();

        let reward = self.world.value();
        (to, reward)
    }

    fn state_space(&self) -> Self::StateSpace {
        let mut space = ProductSpace::empty();
        for _ in 0..self.world.agent_ids.len() {
            space = space + Interval::bounded(0.0, 1000.0);
        }
        for _ in 0..self.world.agent_ids.len() {
            space = space + Interval::bounded(-1.0, 1.0);
        }
        for _ in 0..self.world.agent_ids.len() {
            space = space + Interval::bounded(0.0, 1.0);
        }
        for _ in 0..self.world.agent_ids.len() {
            //space = space + Interval::bounded(0.0, 1.0);
        }
        space
    }

    fn action_space(&self) -> Ordinal {
        let motions = 3 * self.world.agent_ids.len();
        let switch_states = 2 * self.world.rail_network.get_switch_ids().len();
        Ordinal::new(motions + switch_states)
    }
}

#[test]
fn test() {
    let domain = AiDomain::default();
    assert_eq!(domain.world.agents.len(), 2);

    let track_id = domain.world.rail_network.elements.keys().next().unwrap();

    let cloned = domain.clone();
    assert_eq!(cloned.world.agents.len(), 2);
    assert_eq!(
        domain
            .world
            .rail_network
            .elements
            .get(track_id)
            .unwrap()
            .get_position2d(),
        cloned
            .world
            .rail_network
            .elements
            .get(track_id)
            .unwrap()
            .get_position2d()
    );
}
