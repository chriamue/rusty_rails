//! # AI
//!
//! Module that contains functions to train an ai on the simulation.
//!

pub mod agent;
pub mod actor;
pub mod observer;
pub mod default_agent;

#[cfg(feature = "training")]
pub mod ai_domain;
#[cfg(feature = "training")]
pub mod trainer;