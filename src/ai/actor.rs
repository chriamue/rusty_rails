pub trait Actor {
    fn action(&mut self) -> usize;
    fn num_actions(&self) -> usize;
}
