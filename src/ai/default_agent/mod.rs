use rand::Rng;

use crate::ai::agent::Agent;
use crate::ai::actor::Actor;
use crate::ai::observer::Observer;

pub struct DefaultAgent {
    agents: usize,
    switches: usize,
    observation: Vec<f64>,
    last_movement: Vec<usize>,
}

impl DefaultAgent {
    pub fn new(agents: usize, switches: usize) -> DefaultAgent {
        DefaultAgent {
            agents: agents,
            switches: switches,
            observation: vec![],
            last_movement: vec![0; agents]
        }
    }
}

impl Agent for DefaultAgent {}

impl Actor for DefaultAgent {
    fn action(&mut self) -> usize {
        let mut rng = rand::thread_rng();
        let agent: usize = rng.gen_range(0, self.agents);
        let idx = self.switches + 2 * agent + 1;
        match self.observation.get(idx) {
            Some(value) => {
                if *value > 0.0 {
                    agent * 3 + self.last_movement[agent]
                } else if *value == 0.0 {
                    let movement = 1;
                    self.last_movement[agent] = movement;
                    agent * 3 + movement
                } else {
                    let movement = 2;
                    self.last_movement[agent] = movement;
                    agent * 3 + movement
                }
            }
            _ => 0
        }
    }

    fn num_actions(&self) -> usize {
        let motions = 3 * self.agents;
        let switch_states = 2 * self.switches;
        motions + switch_states
    }
}

impl Observer for DefaultAgent {
    fn observe(&mut self, observation: Vec<f64>) {
        self.observation = observation;
    }
}

#[test]
fn test() {
    let agents = 2;
    let switches = 3;
    let mut agent = DefaultAgent::new(agents, switches);
    agent.observe(vec![]);
    assert_eq!(agent.action(), 0);
    assert_eq!(agent.num_actions(), agents * 3 + switches * 2);



    let observation: Vec<f64> = vec![0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0];
    agent.observe(observation);
    assert_eq!(agent.action() % 3, 1);

    let observation: Vec<f64> = vec![0.0, 0.0, 0.0, 1.0, -1.0, 1.0, -1.0];
    agent.observe(observation);
    agent.last_movement = vec![1; agents];
    assert_eq!(agent.action() % 3, 2);

    let observation: Vec<f64> = vec![0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0];
    agent.observe(observation);
    agent.last_movement = vec![1; agents];
    assert_eq!(agent.action() % 3, 1);

}
