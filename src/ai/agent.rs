use crate::ai::actor::Actor;
use crate::ai::observer::Observer;

pub trait Agent: Actor + Observer{
    
}
