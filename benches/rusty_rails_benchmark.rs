use criterion::{Benchmark, criterion_group, criterion_main, Criterion};
use rusty_rails::rails::path_search::PathSearch;
use rusty_rails::rails::rail_network::RailNetwork;
use rusty_rails::simulation::world::World;
use rusty_rails::simulation::agent::Agent;
use rusty_rails::rails::generator::Generator;

pub fn world_benchmark(c: &mut Criterion) {
    c.bench_function("default world", |b| b.iter(|| World::default()));
}

pub fn path_search_benchmark(c: &mut Criterion) {
    let network = RailNetwork::default();
    let track1 = &network.get_element_ids()[0];
    let track2 = &network.get_element_ids()[2];

    c.bench_function("path search", |b| {
        b.iter(|| network.shortest_path(track1, track2).unwrap())
    });
}

#[cfg(feature = "ai")]
pub fn train_benchmark(c: &mut Criterion) {
    use rusty_rails::ai::trainer::{Trainer, TrainingCallback};

    let mut generator = Generator::new(500, 8);
    generator.generate();

    let mut world = World::new(generator.rail_network);

    let agent1 = Agent::new();
    let agent2 = Agent::new();
    world.add_agent(agent1);
    world.add_agent(agent2);
    let mut trainer = Trainer::new(world);
    trainer.max_loops = 10;

    c.bench("training", Benchmark::new("training", move |b| b.iter(|| trainer.train(1, Box::new(move |world, _outer, inner| {}))))
    .sample_size(10));
}

criterion_group!(benches, world_benchmark, path_search_benchmark);

#[cfg(feature = "ai")]
criterion_group!(train_benches,
    train_benchmark
);

#[cfg(not(feature = "ai"))]
criterion_main!(benches);

#[cfg(feature = "ai")]
criterion_main!(benches, train_benches);
