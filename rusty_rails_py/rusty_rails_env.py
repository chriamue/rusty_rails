import numpy as np
import gym
from gym import error, spaces, utils
from gym.utils import seeding

from rusty_rails_py import World


class RustyRailsEnv(gym.Env):
    metadata = {'render.modes': ['human']}

    def __init__(self, world: World):
        super(RustyRailsEnv, self).__init__()
        self.world = world
        self.current_world = world.clone()

        num_agents = self.world.agents_count()
        num_switches = self.world.switches_count()

        self.action_space = spaces.Discrete(3 * num_agents + 2 * num_switches)

        self.observation_space = spaces.Box(
            low=-1000, high=1000, shape=(len(self.world.flat()),), dtype=np.float64)

    def step(self, action):
        self.current_world.update_order(action)
        reward = self.current_world.value()
        done = self.current_world.collision()
        if done:
            self.reset()
        obs = np.array(self.current_world.flat())
        info = dict()
        for agent in self.current_world.agents():
            info[agent] = self.current_world.agent_position(agent)
        return obs, reward, done, info

    def reset(self):
        self.current_world = self.world.clone()
        return np.array(self.current_world.flat())

    def render(self, mode='human'):
        print("Render: ", self.current_world.flat())

    def close(self):
        self.current_world = None
