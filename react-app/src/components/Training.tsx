import React, { useState } from 'react';

interface Props {
}

const Training: React.FC<Props> = () => {

    const [iterations, setIterations] = useState<number>(3);

    const train = () => {
        fetch(`api/train/${iterations}`)
        .then(response => response.text())
        .then(response => {
        }).catch(e => console.log);
    }
    return (
        <div className="Training" >
            <input type="number" placeholder="Iterations" onChange={e => setIterations(Number(e.target.value))} />
            <button onClick={() => train()} >train</button>
        </div>
    );
};

export default Training;
