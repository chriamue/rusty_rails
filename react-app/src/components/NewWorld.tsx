import React, { useState } from 'react';
import { Graphviz } from 'graphviz-react';

interface Props {
}

const NewWorld: React.FC<Props> = () => {

    const [tracks, setTracks] = useState<number>(42);
    const [switches, setSwitches] = useState<number>(3);
    const [agents, setAgents] = useState<number>(3);

    const [dotGraph, setDotGraph] = useState<string>('');

    const generate = () => {
        fetch(`api/new/${tracks}/${switches}/${agents}`)
            .then(response => response.text())
            .then(response => {
            }).catch(e => console.log);

        import('rusty_rails').then(({ generateRailNetwork }) => {
            let content = generateRailNetwork(tracks, switches);
            setDotGraph(content);
            console.log(content);
        });
    }

    return (
        <div className="NewWorld" >
            <input type="number" placeholder="Tracks" onChange={e => setTracks(Number(e.target.value))} />
            <input type="number" placeholder="Switches" onChange={e => setSwitches(Number(e.target.value))} />
            <input type="number" placeholder="Agents" onChange={e => setAgents(Number(e.target.value))} />
            <button onClick={() => generate()} >generate</button>
            {dotGraph.length > 1 ? <Graphviz dot={dotGraph} /> : null}
        </div>
    );
};

export default NewWorld;
