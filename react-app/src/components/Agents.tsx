import React, { useState, useEffect } from 'react';

interface Props {
}

class Agent {
    id: string = "";
    born: string = "";
    distance: number = 0;
    points: number = 0;
}

const Agents: React.FC<Props> = () => {

    const [agents, setAgents] = useState<Agent[]>();

    useEffect(() => {
        const interval = setInterval(() => {
            loadAgents();
        }, 1000);
        return () => clearInterval(interval);
    }, []);

    const loadAgents = () => {
        fetch('api/agents').then(response => response.json())
            .then(response => {
                setAgents(response);
            }).catch(e => console.log);
    }

    const listOfAgents = () => {
        return agents == null ? null : <ul>
            {agents.map((agent, i) => {
                return <li id={"agent_" + i}>
                    id: {agent.id},
                    distance: {agent.distance},
                    points: {agent.points}
                </li>
            })}
        </ul>
    }

    return (
        <div className="Agents" onClick={() => loadAgents()} >
            {listOfAgents()}
        </div>
    );
};

export default Agents;
