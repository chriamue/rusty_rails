import React, { useState, useEffect } from 'react';
import { Graphviz } from 'graphviz-react';

interface Props {
}

const Network: React.FC<Props> = () => {

    const [dotGraph, setDotGraph] = useState<string>('');

    useEffect(() => {
        loadGraph();
    });

    const loadGraph = () => {
        fetch('api/network.dot').then(response => response.text())
            .then(response => {
                if( response.startsWith('dot')){
                    setDotGraph(response);
                }
            }).catch(e => console.log);
    }

    return (
        <div className="Network" onClick={() => loadGraph()} >
            {dotGraph.length > 1 ? <Graphviz dot={dotGraph} options={{width: 1200, height: 800, zoom: true}}/> : null}
        </div>
    );
};

export default Network;
