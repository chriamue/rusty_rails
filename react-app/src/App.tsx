import React from 'react';
import './App.css';
import Network from './components/Network';
import Agents from './components/Agents';
import NewWorld from './components/NewWorld';
import Training from './components/Training';

interface Props {
}

const App: React.FC<Props> = () => {

  const start = () => {
    import('rusty_rails').then(({ main }) => {
      main();
    });
  }


  return (
    <div className="App">
      <Agents />
      <Training />
      <Network />
      <NewWorld />
		  <canvas id="canvas"></canvas>
      <button onClick={() => start()} >start</button>
    </div>
  );
};

export default App;
