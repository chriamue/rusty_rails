# Rusty Rails Notebook

Runs rusty rails as python notebook.

## build

```bash
cargo build --release
cp ../target/release/librusty_rails.so rusty_rails.so
```

## create environment

```bash
virtualenv -p python3 .venv
source .venv/bin/activate
pip install jupyter-notebook
```

## open notebook

```bash
jupyter notebook rusty_rails.ipynb
```
